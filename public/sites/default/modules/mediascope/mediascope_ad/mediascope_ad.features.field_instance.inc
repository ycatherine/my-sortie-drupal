<?php
/**
 * @file
 * mediascope_ad.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function mediascope_ad_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-mediascope_ad-field_mediascope_ad_image'
  $field_instances['node-mediascope_ad-field_mediascope_ad_image'] = array(
    'bundle' => 'mediascope_ad',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => '',
        ),
        'type' => 'image',
        'weight' => 1,
      ),
      'full' => array(
        'label' => 'above',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => '',
        ),
        'type' => 'image',
        'weight' => 2,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_mediascope_ad_image',
    'label' => 'Image',
    'required' => 1,
    'settings' => array(
      'alt_field' => 0,
      'default_image' => 0,
      'file_directory' => 'mediascope_ad/field_mediascope_ad_image',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '',
      'max_resolution' => '',
      'min_resolution' => '',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'preview_image_style' => 'focal_point_preview',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 4,
    ),
  );

  // Exported field_instance: 'node-mediascope_ad-field_mediascope_ad_link'
  $field_instances['node-mediascope_ad-field_mediascope_ad_link'] = array(
    'bundle' => 'mediascope_ad',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'full' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_mediascope_ad_link',
    'label' => 'Lien',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 5,
    ),
  );

  // Exported field_instance: 'node-mediascope_ad-field_mediascope_ad_path'
  $field_instances['node-mediascope_ad-field_mediascope_ad_path'] = array(
    'bundle' => 'mediascope_ad',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Spécifier les pages en utilisant leurs chemins. Saisir un chemin par ligne. Le caractère <strong>«*»</strong> est un caractère de remplacement (aussi appelé joker ou wildcard). 
<em>Exemples de chemins : <strong>cinema</strong> pour la page du cinema et <strong>cinema/*</strong> pour toutes les pages cinemas. <front> représente la page d’accueil.</em>',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 2,
      ),
      'full' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_mediascope_ad_path',
    'label' => 'Chemin(s)',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 6,
    ),
  );

  // Exported field_instance: 'node-mediascope_ad-field_mediascope_ad_region'
  $field_instances['node-mediascope_ad-field_mediascope_ad_region'] = array(
    'bundle' => 'mediascope_ad',
    'default_value' => array(),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 3,
      ),
      'full' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 3,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_mediascope_ad_region',
    'label' => 'Regions',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'apply_chosen' => '',
      ),
      'type' => 'options_buttons',
      'weight' => 1,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Chemin(s)');
  t('Image');
  t('Lien');
  t('Regions');
  t('Spécifier les pages en utilisant leurs chemins. Saisir un chemin par ligne. Le caractère <strong>«*»</strong> est un caractère de remplacement (aussi appelé joker ou wildcard). 
<em>Exemples de chemins : <strong>cinema</strong> pour la page du cinema et <strong>cinema/*</strong> pour toutes les pages cinemas. <front> représente la page d’accueil.</em>');

  return $field_instances;
}
