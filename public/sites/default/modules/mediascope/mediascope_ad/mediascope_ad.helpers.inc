<?php
/**
 * @file
 * Helpers for the Mediascope ad module.
 */

/**
 * Get the regions from the Mediascope region field.
 *
 * @return array.
 *   An array of regions.
 */
function _mediascope_ad_regions() {
  return list_allowed_values(field_info_field(MEDIASCOPE_AD_FIELD_REGIONS));
}


/**
 * Get all ads from a region.
 *
 * @param string $region.
 *   The region name from field_mediascope_ad_regions.
 *
 * @return array.
 *   An array of node objects, otherwise FALSE.
 */
function _mediascope_ad_get_ads_from_region_name($region) {
  $query = new EntityFieldQuery();
  $query
    ->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', MEDIASCOPE_AD_BUNDLE)
    ->fieldCondition(MEDIASCOPE_AD_FIELD_REGIONS, 'value', $region, '=')
    ->propertyCondition('status', 1)
    ->addTag('node_access');
  $result = $query->execute();

  return isset($result['node']) ? node_load_multiple(array_keys($result['node'])) : FALSE;
}


/**
 * Get a random advert available on the page.
 *
 * @param array $nodes.
 *   An array of node objects.
 *
 * @return object.
 *   A random node object, otherwise FALSE.
 */
function _mediascope_ad_get_random_ad($nodes) {

  $matching_ads = array();

  foreach ($nodes as $node) {
    // Add the node to the array if the node is matching the path.
    if (_mediascope_ad_path_is_a_match($node)) {
      $matching_ads[] = $node;
    }
  }

  if (empty($matching_ads)) {
    return FALSE;
  }

  return $matching_ads[array_rand($matching_ads, 1)];
}


/**
 * Check if the path is matching.
 *
 * @param object $node.
 *   A node object.
 *
 * @return bool.
 *   TRUE if the path matches a pattern, FALSE otherwise.
 */
function _mediascope_ad_path_is_a_match($node) {

  // Retrieve the value of the path field for the current node,
  // or return FALSE if we don't have a value.
  if (!$path_value = _mediascope_ad_get_path_values($node)) {
    return FALSE;
  }

  $current_path = drupal_strtolower(drupal_get_path_alias(current_path()));

  return drupal_match_path($current_path, drupal_strtolower($path_value));
}


/**
 * Get the value(s) of field_mediascope_ad_path.
 *
 * @param object $entity.
 *   The entity containing the data to be displayed.
 *
 * @return string.
 *   Return the field_mediascope_ad_path value, otherwise FALSE.
 */
function _mediascope_ad_get_path_values($entity) {

  if (!$path = field_get_items('node', $entity, MEDIASCOPE_AD_FIELD_PATH)) {
    return FALSE;
  }

  return reset($path)['value'];
}


/**
 * A wrapper for the block content.
 *
 * @param string $region.
 *   The region name.
 *
 * @return array.
 *   An array as expected by drupal_render(), otherwise FALSE.
 */
function _mediascope_ad_block_content($region) {

  if (($nodes = _mediascope_ad_get_ads_from_region_name($region)) == FALSE) {
    return FALSE;
  }

  if (($node = _mediascope_ad_get_random_ad($nodes)) == FALSE) {
    return FALSE;
  }

  return node_view($node, 'full');
}
