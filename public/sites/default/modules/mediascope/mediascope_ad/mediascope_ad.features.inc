<?php
/**
 * @file
 * mediascope_ad.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function mediascope_ad_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function mediascope_ad_node_info() {
  $items = array(
    'mediascope_ad' => array(
      'name' => t('Mediascope Publicité'),
      'base' => 'node_content',
      'description' => t('Ajouter une publicité.'),
      'has_title' => '1',
      'title_label' => t('Titre administratif'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
