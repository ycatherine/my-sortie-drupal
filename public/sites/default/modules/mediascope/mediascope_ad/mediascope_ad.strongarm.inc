<?php
/**
 * @file
 * mediascope_ad.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function mediascope_ad_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'additional_settings__active_tab_mediascope_ad';
  $strongarm->value = 'edit-webform';
  $export['additional_settings__active_tab_mediascope_ad'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'diff_enable_revisions_page_node_mediascope_ad';
  $strongarm->value = 0;
  $export['diff_enable_revisions_page_node_mediascope_ad'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'diff_show_preview_changes_node_mediascope_ad';
  $strongarm->value = 0;
  $export['diff_show_preview_changes_node_mediascope_ad'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'diff_view_mode_preview_node_mediascope_ad';
  $strongarm->value = '';
  $export['diff_view_mode_preview_node_mediascope_ad'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_node__mediascope_ad';
  $strongarm->value = array(
    'view_modes' => array(
      'full' => array(
        'custom_settings' => TRUE,
      ),
      'teaser' => array(
        'custom_settings' => FALSE,
      ),
      'rss' => array(
        'custom_settings' => FALSE,
      ),
      'diff_standard' => array(
        'custom_settings' => FALSE,
      ),
      'banner' => array(
        'custom_settings' => FALSE,
      ),
      'card' => array(
        'custom_settings' => FALSE,
      ),
      'address' => array(
        'custom_settings' => FALSE,
      ),
      'map' => array(
        'custom_settings' => FALSE,
      ),
      'avatar_content' => array(
        'custom_settings' => FALSE,
      ),
      'thumbnail' => array(
        'custom_settings' => FALSE,
      ),
      'lightbox' => array(
        'custom_settings' => FALSE,
      ),
      'jumbotron' => array(
        'custom_settings' => FALSE,
      ),
      'movie_card' => array(
        'custom_settings' => FALSE,
      ),
      'bubble' => array(
        'custom_settings' => FALSE,
      ),
      'token' => array(
        'custom_settings' => FALSE,
      ),
    ),
    'extra_fields' => array(
      'form' => array(
        'title' => array(
          'weight' => '0',
        ),
        'path' => array(
          'weight' => '3',
        ),
        'rabbit_hole' => array(
          'weight' => '2',
        ),
      ),
      'display' => array(),
    ),
  );
  $export['field_bundle_settings_node__mediascope_ad'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'language_content_type_mediascope_ad';
  $strongarm->value = '0';
  $export['language_content_type_mediascope_ad'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_mediascope_ad';
  $strongarm->value = array();
  $export['menu_options_mediascope_ad'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_mediascope_ad';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_mediascope_ad'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_mediascope_ad';
  $strongarm->value = array();
  $export['node_options_mediascope_ad'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_mediascope_ad';
  $strongarm->value = '0';
  $export['node_preview_mediascope_ad'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_mediascope_ad';
  $strongarm->value = 0;
  $export['node_submitted_mediascope_ad'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'publish_button_content_type_mediascope_ad';
  $strongarm->value = 1;
  $export['publish_button_content_type_mediascope_ad'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'redirect_setting_name_mediascope_ad';
  $strongarm->value = 'rh_node_redirect';
  $export['redirect_setting_name_mediascope_ad'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'rh_module_mediascope_ad';
  $strongarm->value = 'rh_node';
  $export['rh_module_mediascope_ad'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'rh_node_action_mediascope_ad';
  $strongarm->value = '1';
  $export['rh_node_action_mediascope_ad'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'rh_node_override_mediascope_ad';
  $strongarm->value = 0;
  $export['rh_node_override_mediascope_ad'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'rh_node_redirect_mediascope_ad';
  $strongarm->value = '';
  $export['rh_node_redirect_mediascope_ad'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'rh_node_redirect_response_mediascope_ad';
  $strongarm->value = '301';
  $export['rh_node_redirect_response_mediascope_ad'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'save_continue_mediascope_ad';
  $strongarm->value = 'Enregistrer et ajouter les champs';
  $export['save_continue_mediascope_ad'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'simplify_nodes_mediascope_ad';
  $strongarm->value = array(
    0 => 'author',
    1 => 'format',
    2 => 'options',
    3 => 'revision',
    4 => 'menu',
    5 => 'path',
  );
  $export['simplify_nodes_mediascope_ad'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'webform_node_mediascope_ad';
  $strongarm->value = 0;
  $export['webform_node_mediascope_ad'] = $strongarm;

  return $export;
}
