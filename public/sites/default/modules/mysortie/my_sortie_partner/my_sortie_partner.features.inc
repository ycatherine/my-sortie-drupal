<?php
/**
 * @file
 * my_sortie_partner.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function my_sortie_partner_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function my_sortie_partner_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_fe_nodequeue_export_fields().
 */
function my_sortie_partner_fe_nodequeue_export_fields() {
  $nodequeues = array();

  // Exported nodequeues: partners
  $nodequeues['partners'] = array(
    'name' => 'partners',
    'title' => 'Partenaires',
    'subqueue_title' => '',
    'size' => 0,
    'link' => '',
    'link_remove' => '',
    'owner' => 'nodequeue',
    'show_in_ui' => 1,
    'show_in_tab' => 1,
    'show_in_links' => 0,
    'reference' => 0,
    'reverse' => 0,
    'insert_at_front' => 0,
    'i18n' => 0,
    'subqueues' => 1,
    'types' => array(
      0 => 'partner',
    ),
    'roles' => array(),
    'count' => 0,
  );

  return $nodequeues;
}

/**
 * Implements hook_node_info().
 */
function my_sortie_partner_node_info() {
  $items = array(
    'partner' => array(
      'name' => t('Partenaire'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Titre administratif'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
