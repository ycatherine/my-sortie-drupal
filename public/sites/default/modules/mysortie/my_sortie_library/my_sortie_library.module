<?php
/**
 * @file
 * Contains Drupal Hooks for the My Sortie Library module.
 */

/**
 * Implements hook_libraries_info().
 */
function my_sortie_library_libraries_info() {
  $libraries = array();

  // Information about the slick libary.
  $libraries['slick'] = array(
    'name' => 'Slick',
    'vendor url' => 'http://kenwheeler.github.io/slick/',
    'download url' => 'https://github.com/kenwheeler/slick/archive/master.zip',
    'version arguments' => array(
      'file' => 'slick.js',
      'pattern' => '/Version: (\d+\.+\d+)/',
      'lines' => 9,
    ),
    'files' => array(
      'js' => array(
        'slick.min.js',
      ),
      'css' => array(
        'slick.css',
      ),
    ),
  );

  // Information about the Magnific Popup Library.
  $libraries['magnific_popup'] = array(
    'name' => 'Magnific Popup',
    'vendor url' => 'http://dimsemenov.com/plugins/magnific-popup/',
    'download url' => 'https://github.com/dimsemenov/Magnific-Popup/archive/master.zip',
    'version arguments' => array(
      'file' => 'jquery.magnific-popup.js',
      'pattern' => '/! Magnific Popup - v(\d+\.+\d+)/',
      'lines' => 1,
    ),
    'files' => array(
      'js' => array(
        'jquery.magnific-popup.js',
      ),
      'css' => array(
        'magnific-popup.css',
      ),
    ),
  );

  // Information about Vue.js
  $libraries['vuejs'] = array(
    'name' => 'Vue.js',
    'vendor url' => 'http://vuejs.org/',
    'download url' => 'https://raw.githubusercontent.com/yyx990803/vue/0.12.9/dist/vue.min.js',
    'version arguments' => array(
      'file' => 'vue.min.js',
      'pattern' => '/Vue.js v(\d+\.+\d+)/',
      'lines' => 2,
    ),
    'files' => array(
      'js' => array(
        'vue.min.js',
      ),
    ),
  );

  // Information about the Slidebars Library.
  $libraries['slidebars'] = array(
    'name' => 'Slidebars',
    'vendor url' => 'http://plugins.adchsm.me/slidebars/',
    'download url' => 'https://github.com/adchsm/Slidebars/archive/0.10.3.zip',
    'version arguments' => array(
      'file' => 'slidebars.min.js',
      'pattern' => '/Slidebars (\d+\.+\d+)/',
      'lines' => 1,
    ),
    'files' => array(
      'js' => array(
        'slidebars.min.js',
      ),
      'css' => array(
        'slidebars.min.css',
      ),
    ),
  );

  // Information about the Sticky Library.
  $libraries['sticky'] = array(
    'name' => 'Sticky',
    'vendor url' => 'http://stickyjs.com',
    'download url' => 'https://github.com/garand/sticky/archive/master.zip',
    'version arguments' => array(
      'file' => 'jquery.sticky.js',
      'pattern' => '/Sticky Plugin v(\d+\.+\d+)/',
      'lines' => 1,
    ),
    'files' => array(
      'js' => array(
        'jquery.sticky.js',
      ),
    ),
  );

  return $libraries;
}

/**
 * Short-circuit the version argument.
 */
function _my_sortie_library_short_circuit_version() {
  return TRUE;
}
