<?php
/**
 * @file
 * Configuration for My Sortie Slideshow.
 */

/**
 * List of blocks that are considered as slides.
 *
 * @return array.
 *   Return an array of block deltas.
 */
function _my_sortie_slideshow_config_block_deltas_slides() {

  // Add more deltas to the configuration.
  return array(
    'nodequeue_1-index_slideshow',
    'nodequeue_2-cinema_slideshow',
  );

}
