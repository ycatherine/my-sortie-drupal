/**
 * @file
 * Implements slick slider library.
 */

(function ($){

  /**
   * Slick slider.
   *
   * @see http://kenwheeler.github.io/slick/
   */
  Drupal.behaviors.mySortieSlideshowSlick = {
    attach: function (context, settings) {

      var $slideshowContext = $('.view--slideshow', context);

      $('.view-content', $slideshowContext).once('view--slideshow').slick({
        dots: true,
        arrows: false,
        fade: false,
        autoplay: true,
        autoplaySpeed: 5000,
        slidesToShow: 1,
        adaptiveHeight: true
      });

    }
  };

})(jQuery);