<?php
/**
 * @file
 * Helper functions and wrappers for My Sortie Slideshow.
 */

/**
 * Wrapper to determine if the block is a slide.
 *
 * @param object $block.
 *   The block object,.
 *
 * @return boolean.
 *   TRUE if block delta is in config block deltas, otherwise FALSE.
 */
function _my_sortie_slideshow_block_is_slide($block) {
  // Load the module configuration.
  module_load_include('inc', 'my_sortie_slideshow', 'my_sortie_slideshow.config');

  return in_array($block->delta, _my_sortie_slideshow_config_block_deltas_slides());
}

/**
 * Attach slick and custom js to a block.
 *
 * @param array $data.
 *   The data as returned from the hook_block_view() implementation of the
 *   module that defined the block.
 *
 * @return array.
 *   Returns a render array
 */
function _my_sortie_slideshow_attach_slick_js_library(&$data) {
  $data['content']['#attached']['libraries_load'][] = array('slick');
  $data['content']['#attached']['js'][] = array(
    'data' => drupal_get_path('module', 'my_sortie_slideshow') . '/js/my_sortie_slideshow.slick.js',
    'type' => 'file',
  );
}
