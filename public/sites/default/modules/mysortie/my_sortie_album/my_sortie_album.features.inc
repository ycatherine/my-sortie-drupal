<?php
/**
 * @file
 * my_sortie_album.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function my_sortie_album_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function my_sortie_album_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function my_sortie_album_node_info() {
  $items = array(
    'album' => array(
      'name' => t('Album'),
      'base' => 'node_content',
      'description' => t('Créer un nouvel album <strong>retour en images</strong>.'),
      'has_title' => '1',
      'title_label' => t('Titre de l\'album'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

/**
 * Implements hook_view_mode_page_pattern_default().
 */
function my_sortie_album_view_mode_page_pattern_default() {
  $vmp_config = array();

  $vmp_config[] = array(
    0 => array(
      'content_type' => 'album',
      'view_mode' => 'default',
      'url_pattern' => '',
      'entity_type' => 'node',
      'show_title' => 0,
      'title' => '',
    ),
  );
  $vmp_config[] = array(
    0 => array(
      'content_type' => 'album',
      'view_mode' => 'full',
      'url_pattern' => '',
      'entity_type' => 'node',
      'show_title' => 0,
      'title' => '',
    ),
  );
  $vmp_config[] = array(
    0 => array(
      'content_type' => 'album',
      'view_mode' => 'lightbox',
      'url_pattern' => 'sorties/retours/%/lightbox',
      'entity_type' => 'node',
      'show_title' => 0,
      'title' => '',
    ),
  );
  $vmp_config[] = array(
    0 => array(
      'content_type' => 'album',
      'view_mode' => 'thumbnail',
      'url_pattern' => '',
      'entity_type' => 'node',
      'show_title' => 0,
      'title' => '',
    ),
  );
  return $vmp_config;
}
