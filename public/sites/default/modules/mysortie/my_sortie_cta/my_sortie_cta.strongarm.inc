<?php
/**
 * @file
 * my_sortie_cta.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function my_sortie_cta_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_node__call_to_action';
  $strongarm->value = array(
    'view_modes' => array(
      'panel' => array(
        'custom_settings' => TRUE,
      ),
      'full' => array(
        'custom_settings' => FALSE,
      ),
      'teaser' => array(
        'custom_settings' => FALSE,
      ),
      'rss' => array(
        'custom_settings' => FALSE,
      ),
      'banner' => array(
        'custom_settings' => FALSE,
      ),
      'card' => array(
        'custom_settings' => FALSE,
      ),
      'address' => array(
        'custom_settings' => FALSE,
      ),
      'map' => array(
        'custom_settings' => FALSE,
      ),
      'avatar_content' => array(
        'custom_settings' => FALSE,
      ),
      'thumbnail' => array(
        'custom_settings' => FALSE,
      ),
      'lightbox' => array(
        'custom_settings' => FALSE,
      ),
      'jumbotron' => array(
        'custom_settings' => FALSE,
      ),
      'movie_card' => array(
        'custom_settings' => FALSE,
      ),
      'bubble' => array(
        'custom_settings' => FALSE,
      ),
      'token' => array(
        'custom_settings' => FALSE,
      ),
    ),
    'extra_fields' => array(
      'form' => array(
        'metatags' => array(
          'weight' => '6',
        ),
        'title' => array(
          'weight' => '0',
        ),
        'path' => array(
          'weight' => '5',
        ),
        'rabbit_hole' => array(
          'weight' => '2',
        ),
      ),
      'display' => array(),
    ),
  );
  $export['field_bundle_settings_node__call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'language_content_type_call_to_action';
  $strongarm->value = '0';
  $export['language_content_type_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_call_to_action';
  $strongarm->value = array();
  $export['menu_options_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_call_to_action';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_call_to_action';
  $strongarm->value = array(
    0 => 'status',
  );
  $export['node_options_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_call_to_action';
  $strongarm->value = '0';
  $export['node_preview_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_call_to_action';
  $strongarm->value = 0;
  $export['node_submitted_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'publish_button_content_type_call_to_action';
  $strongarm->value = 1;
  $export['publish_button_content_type_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'redirect_setting_name_call_to_action';
  $strongarm->value = 'rh_node_redirect';
  $export['redirect_setting_name_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'rh_module_call_to_action';
  $strongarm->value = 'rh_node';
  $export['rh_module_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'rh_node_action_call_to_action';
  $strongarm->value = '1';
  $export['rh_node_action_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'rh_node_override_call_to_action';
  $strongarm->value = 0;
  $export['rh_node_override_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'rh_node_redirect_call_to_action';
  $strongarm->value = '';
  $export['rh_node_redirect_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'rh_node_redirect_response_call_to_action';
  $strongarm->value = '301';
  $export['rh_node_redirect_response_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'simplify_nodes_call_to_action';
  $strongarm->value = array(
    0 => 'author',
    1 => 'format',
    2 => 'options',
    3 => 'revision',
    4 => 'menu',
    5 => 'path',
    6 => 'metatag',
  );
  $export['simplify_nodes_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'webform_node_call_to_action';
  $strongarm->value = 0;
  $export['webform_node_call_to_action'] = $strongarm;

  return $export;
}
