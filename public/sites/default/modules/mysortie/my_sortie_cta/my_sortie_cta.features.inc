<?php
/**
 * @file
 * my_sortie_cta.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function my_sortie_cta_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function my_sortie_cta_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_fe_nodequeue_export_fields().
 */
function my_sortie_cta_fe_nodequeue_export_fields() {
  $nodequeues = array();

  // Exported nodequeues: call_to_action
  $nodequeues['call_to_action'] = array(
    'name' => 'call_to_action',
    'title' => 'Appel à l\'action',
    'subqueue_title' => '',
    'size' => 6,
    'link' => '',
    'link_remove' => '',
    'owner' => 'nodequeue',
    'show_in_ui' => 1,
    'show_in_tab' => 1,
    'show_in_links' => 0,
    'reference' => 0,
    'reverse' => 0,
    'insert_at_front' => 0,
    'i18n' => 0,
    'subqueues' => 1,
    'types' => array(
      0 => 'call_to_action',
    ),
    'roles' => array(),
    'count' => 0,
  );

  return $nodequeues;
}

/**
 * Implements hook_node_info().
 */
function my_sortie_cta_node_info() {
  $items = array(
    'call_to_action' => array(
      'name' => t('Appel à action'),
      'base' => 'node_content',
      'description' => t('Liens en page d\'accueil.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
