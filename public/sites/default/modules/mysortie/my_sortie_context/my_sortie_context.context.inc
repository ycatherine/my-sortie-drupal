<?php
/**
 * @file
 * my_sortie_context.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function my_sortie_context_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'blocks_cinema';
  $context->description = 'Blocks on the cinema landing page.';
  $context->tag = 'blocks';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'cinema' => 'cinema',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-nodequeue_2-cinema_slideshow' => array(
          'module' => 'views',
          'delta' => 'nodequeue_2-cinema_slideshow',
          'region' => 'banner',
          'weight' => '-10',
        ),
        'my_sortie_cinema-this_week' => array(
          'module' => 'my_sortie_cinema',
          'delta' => 'this_week',
          'region' => 'content',
          'weight' => '-10',
        ),
        'webform-client-block-12' => array(
          'module' => 'webform',
          'delta' => 'client-block-12',
          'region' => 'content',
          'weight' => '-9',
        ),
        'my_sortie_cinema-upcoming_movies' => array(
          'module' => 'my_sortie_cinema',
          'delta' => 'upcoming_movies',
          'region' => 'content_suffix',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Blocks on the cinema landing page.');
  t('blocks');
  $export['blocks_cinema'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'blocks_film_node';
  $context->description = 'Blocks on the film node full view mode.';
  $context->tag = 'blocks';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'film' => 'film',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'webform-client-block-12' => array(
          'module' => 'webform',
          'delta' => 'client-block-12',
          'region' => 'content',
          'weight' => '5',
        ),
        'my_sortie_cinema-upcoming_movies' => array(
          'module' => 'my_sortie_cinema',
          'delta' => 'upcoming_movies',
          'region' => 'content_suffix',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Blocks on the film node full view mode.');
  t('blocks');
  $export['blocks_film_node'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'blocks_front';
  $context->description = 'Manage blocks on the front page.';
  $context->tag = 'blocks';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-nodequeue_1-index_slideshow' => array(
          'module' => 'views',
          'delta' => 'nodequeue_1-index_slideshow',
          'region' => 'banner',
          'weight' => '-21',
        ),
        'views-cta-block_primary_cta' => array(
          'module' => 'views',
          'delta' => 'cta-block_primary_cta',
          'region' => 'banner',
          'weight' => '-20',
        ),
        'quicktabs-events_listing' => array(
          'module' => 'quicktabs',
          'delta' => 'events_listing',
          'region' => 'content',
          'weight' => '-10',
        ),
        'webform-client-block-12' => array(
          'module' => 'webform',
          'delta' => 'client-block-12',
          'region' => 'content',
          'weight' => '-9',
        ),
        'views-album-block_album' => array(
          'module' => 'views',
          'delta' => 'album-block_album',
          'region' => 'content_suffix',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Manage blocks on the front page.');
  t('blocks');
  $export['blocks_front'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'blocks_section_cinema';
  $context->description = 'Blocks in the cinema section';
  $context->tag = 'blocks';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'cinema' => 'cinema',
        'cinema/*' => 'cinema/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'menu-menu-menu-cinema' => array(
          'module' => 'menu',
          'delta' => 'menu-menu-cinema',
          'region' => 'navigation',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Blocks in the cinema section');
  t('blocks');
  $export['blocks_section_cinema'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'blocks_sortie_section';
  $context->description = '';
  $context->tag = 'blocks';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'album' => 'album',
        'event' => 'event',
        'location' => 'location',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
    'taxonomy_term' => array(
      'values' => array(
        'event_types' => 'event_types',
        'location_type' => 'location_type',
      ),
      'options' => array(
        'term_form' => '0',
      ),
    ),
    'views' => array(
      'values' => array(
        'album' => 'album',
        'album:album_page' => 'album:album_page',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'menu-menu-sorties' => array(
          'module' => 'menu',
          'delta' => 'menu-sorties',
          'region' => 'navigation',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('blocks');
  $export['blocks_sortie_section'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'mediascope_ads';
  $context->description = 'Manages ads on all pages.';
  $context->tag = 'advert';
  $context->conditions = array(
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'mediascope_ad-header' => array(
          'module' => 'mediascope_ad',
          'delta' => 'header',
          'region' => 'ad_top',
          'weight' => '-10',
        ),
        'mediascope_ad-footer' => array(
          'module' => 'mediascope_ad',
          'delta' => 'footer',
          'region' => 'ad_bottom',
          'weight' => '-10',
        ),
        'mediascope_ad-sidebar' => array(
          'module' => 'mediascope_ad',
          'delta' => 'sidebar',
          'region' => 'ad_sidebar',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Manages ads on all pages.');
  t('advert');
  $export['mediascope_ads'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'page_title';
  $context->description = 'Manages the page title block.';
  $context->tag = 'Page Title';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'page' => 'page',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
    'path' => array(
      'values' => array(
        'lieux' => 'lieux',
        'cinema/salles' => 'cinema/salles',
      ),
    ),
    'taxonomy_term' => array(
      'values' => array(
        'event_types' => 'event_types',
      ),
      'options' => array(
        'term_form' => '0',
      ),
    ),
    'views' => array(
      'values' => array(
        'events_listing' => 'events_listing',
        'events_listing:event_taxonomy_pages' => 'events_listing:event_taxonomy_pages',
        'game' => 'game',
        'game:page' => 'game:page',
        'partners' => 'partners',
        'partners:page_partners' => 'partners:page_partners',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'my_sortie_block-page_title' => array(
          'module' => 'my_sortie_block',
          'delta' => 'page_title',
          'region' => 'banner',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Manages the page title block.');
  t('Page Title');
  $export['page_title'] = $context;

  return $export;
}
