<?php
/**
 * @file
 * Contains helpers functions related to images.
 */

/**
 * Retrieves the absolute URL where a style image can be downloaded, suitable
 * for use in an <img> tag. Requesting the URL will cause the image to be
 * created.
 *
 * @param string $style_name.
 *   The name of the style to be used with this image.
 *
 * @param object $entity.
 *   The entity containing the data to be displayed.

 * @param string $field_name.
 *  The field to be displayed.
 *
 * @return string.
 *   Returns the URL for an image derivative given a style and image path.
 */
function _my_sortie_image_get_style_url($style_name, $entity, $field_name) {

  module_load_include('inc', 'my_sortie_field', 'my_sortie_field.helpers');
  $uri  = _my_sortie_field_get_value($entity, $field_name, 'uri');

  return ($uri) ? image_style_url($style_name, $uri) : FALSE;
}
