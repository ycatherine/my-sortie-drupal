<?php
/**
 * @file
 * my_sortie_image.features.inc
 */

/**
 * Implements hook_image_default_styles().
 */
function my_sortie_image_image_default_styles() {
  $styles = array();

  // Exported image style: affiche_thumb.
  $styles['affiche_thumb'] = array(
    'label' => 'Affiche Thumb (400x534)',
    'effects' => array(
      11 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 400,
          'height' => 534,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: avatar.
  $styles['avatar'] = array(
    'label' => 'Avatar (300x300)',
    'effects' => array(
      6 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 300,
          'height' => 300,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: banner.
  $styles['banner'] = array(
    'label' => 'Banner (1400x550)',
    'effects' => array(
      5 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 1400,
          'height' => 550,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: banner_large.
  $styles['banner_large'] = array(
    'label' => 'Banner Large (2560x860)',
    'effects' => array(
      13 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 2560,
          'height' => 860,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: banner_thumb.
  $styles['banner_thumb'] = array(
    'label' => 'Banner Thumb (640x215)',
    'effects' => array(
      12 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 640,
          'height' => 215,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: card.
  $styles['card'] = array(
    'label' => 'Card (768x475)',
    'effects' => array(
      4 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 768,
          'height' => 475,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: cta.
  $styles['cta'] = array(
    'label' => 'CTA (580x480)',
    'effects' => array(
      15 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 580,
          'height' => 480,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: cta_large.
  $styles['cta_large'] = array(
    'label' => 'CTA (1160x960)',
    'effects' => array(
      16 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 1160,
          'height' => 960,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: slideshow.
  $styles['slideshow'] = array(
    'label' => 'Slideshow (2560x1000)',
    'effects' => array(
      2 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 2560,
          'height' => 1000,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: slideshow_2016.
  $styles['slideshow_2016'] = array(
    'label' => 'Slideshow 2016 (1160x960)',
    'effects' => array(
      14 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 1160,
          'height' => 960,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: slideshow_thumb.
  $styles['slideshow_thumb'] = array(
    'label' => 'Slideshow Thumb (640x395)',
    'effects' => array(
      3 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 640,
          'height' => 395,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}
