<?php
/**
 * @file
 * Contains helpers for the Cinema module.
 */

/**
 * Get all nodes from the film content type and load the nodes.
 *
 * @return object.
 *   Returns published node objects.
 */
function _my_sortie_cinema_get_films() {
  $query = new EntityFieldQuery();

  $query
    ->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'film')
    ->propertyCondition('status', 1);
  $result = $query->execute();

  return node_load_multiple(array_keys($result['node']));
}

/**
 *
 */
function _my_sortie_cinema_get_this_week_movies() {
  $sqid = nodequeue_load_queue_by_name('this_week_movies')->qid;
  return nodequeue_load_nodes($sqid, FALSE, 0, 0);
}


/**
 *
 */
function _my_sortie_cinema_get_upcoming_movies() {
  $sqid = nodequeue_load_queue_by_name('upcoming_movies')->qid;
  return nodequeue_load_nodes($sqid, FALSE, 0, 0);
}


/**
 * Builds our object for Drupal.settings.
 *
 * @param array $nodes.
 *   An array of node objects.
 *
 * @return array.
 *   An array of dat.
 */
function _my_sortie_cinema_data($nodes) {
  module_load_include('inc', 'my_sortie_image', 'my_sortie_image.helpers');
  $data = array();

  foreach ($nodes as $node) {
    $title   = $node->title;
    $path    = drupal_get_path_alias('node/' . $node->nid);
    $image   = _my_sortie_image_get_style_url('affiche_thumb', $node, 'field_image');
    $cinemas = _my_sortie_field_get_entity_reference_title($node, 'field_locations');

    // Build our object for Drupal.settings.
    $data[] = array(
      'title'   => $title,
      'path'     => $path,
      'image'   => $image ? $image : '',
      'cinemas' => $cinemas ? $cinemas : '',
    );
  }

  return $data;
}


/**
 * Get the cinemas.
 *
 * @return array.
 *   An array of node objects indexed by nid.
 */
function _my_sortie_cinema_get_cinemas() {

  $query = new EntityFieldQuery();

  $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'location')
    ->propertyCondition('status', 1)
    ->addTag('node_access')
    ->fieldCondition('field_location_types', 'tid', MY_SORTIE_CINEMA_TID);

  $result = $query->execute();

  if (!($nids = array_keys($result['node']))) {
    return FALSE;
  }

  return node_load_multiple($nids);
}


/**
 * Get all movies from a cinema.
 *
 * @param int $location_id.
 *   The node id of the location.
 *
 * @return array.
 *   An array of node objects, otherwise FALSE.
 */
function _my_sortie_get_movies_from_location_id($location_id) {

  $movies = new EntityFieldQuery();

  $movies->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'film')
    ->propertyCondition('status', 1)
    ->addTag('node_access')
    ->fieldCondition('field_locations', 'target_id', $location_id);

  if (empty($results = $movies->execute())) {
    return FALSE;
  }

  return node_load_multiple(array_keys($results['node']));
}
