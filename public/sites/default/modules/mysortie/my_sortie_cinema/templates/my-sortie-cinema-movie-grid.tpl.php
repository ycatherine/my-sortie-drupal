<?php
/**
 * @file
 * Custom implementation of the my_sortie_cinema_movie_grid theme function.
 */
?>
<div class="movie-grid">
  <h2 class="movie-grid__title"><?php print $title; ?></h2>
  <div class="movie-grid__wrapper"><?php print render($content); ?></div>
</div>
<!-- my-sortie-cinema-movie-grid.tpl.php -->