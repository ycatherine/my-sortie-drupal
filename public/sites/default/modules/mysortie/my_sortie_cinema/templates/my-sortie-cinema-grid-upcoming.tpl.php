<?php
/**
 * @file
 * Vuejs Component template.
 */
?>
<script type="text/x-template" id="movie-grid-template-upcoming">

  <div class="movie-card" v-repeat="entry: data | filterBy filterKey">
    <a href="/{{entry.path}}" title="{{entry.title}}" class="movie-card__link">
      <img class="movie-card__image" v-attr="src:entry.image" title="{{entry.title}}" />
      <h3 class="movie-card__title">{{entry.title}}</h3>
    </a>
  </div>

</script>

<!-- Movie Grid element -->
<div id="movie-grid-upcoming" class="movie-grid">

  <div class="movie-grid__header">
    <?php if ($title): ?>
      <h2 class="movie-grid__title"><?php print $title; ?></h2>
    <?php endif; ?>

    <?php if ($placeholder): ?>
      <input type="text" class="movie-grid__search" placeholder="<?php print $placeholder; ?>" name="query" v-model="searchQuery">
    <?php endif; ?>
  </div>

  <div class="movie-grid__wrapper">
    <movie-grid-upcoming data="{{gridData}}" filter-key="{{searchQuery}}"></movie-grid-upcoming>
  </div>

</div>