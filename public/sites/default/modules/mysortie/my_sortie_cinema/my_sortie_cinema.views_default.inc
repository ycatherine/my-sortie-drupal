<?php
/**
 * @file
 * my_sortie_cinema.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function my_sortie_cinema_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'cinema_coming_soon';
  $view->description = 'Display a list of all nodes in queue \'Index - Slideshow\'';
  $view->tag = 'nodequeue';
  $view->base_table = 'node';
  $view->human_name = 'Cinema  - Coming soon';
  $view->core = 0;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Defaults */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->display->display_options['title'] = 'Prochainement en salle';
  $handler->display->display_options['css_class'] = 'view--slideshow margin-bottom';
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'load_more';
  $handler->display->display_options['pager']['options']['items_per_page'] = '12';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['pager']['options']['tags']['first'] = '« premier';
  $handler->display->display_options['pager']['options']['tags']['previous'] = '‹ précédent';
  $handler->display->display_options['pager']['options']['tags']['next'] = 'suivant ›';
  $handler->display->display_options['pager']['options']['tags']['last'] = 'dernier »';
  $handler->display->display_options['pager']['options']['more_button_text'] = 'Plus de films';
  $handler->display->display_options['pager']['options']['effects']['type'] = 'fade';
  $handler->display->display_options['pager']['options']['effects']['speed'] = 'slow';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'node';
  $handler->display->display_options['row_options']['view_mode'] = 'jumbotron';
  /* Relationship: Nodequeue: Queue */
  $handler->display->display_options['relationships']['nodequeue_rel']['id'] = 'nodequeue_rel';
  $handler->display->display_options['relationships']['nodequeue_rel']['table'] = 'node';
  $handler->display->display_options['relationships']['nodequeue_rel']['field'] = 'nodequeue_rel';
  $handler->display->display_options['relationships']['nodequeue_rel']['required'] = TRUE;
  $handler->display->display_options['relationships']['nodequeue_rel']['limit'] = 1;
  $handler->display->display_options['relationships']['nodequeue_rel']['names'] = array(
    'upcoming_movies' => 'upcoming_movies',
    'index_slideshow' => 0,
    'cinema_slideshow' => 0,
    'this_week_movies' => 0,
  );
  /* Champ: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  /* Sort criterion: Nodequeue: Position */
  $handler->display->display_options['sorts']['position']['id'] = 'position';
  $handler->display->display_options['sorts']['position']['table'] = 'nodequeue_nodes';
  $handler->display->display_options['sorts']['position']['field'] = 'position';
  $handler->display->display_options['sorts']['position']['relationship'] = 'nodequeue_rel';
  /* Filter criterion: Content: Published or admin */
  $handler->display->display_options['filters']['status_extra']['id'] = 'status_extra';
  $handler->display->display_options['filters']['status_extra']['table'] = 'node';
  $handler->display->display_options['filters']['status_extra']['field'] = 'status_extra';
  $handler->display->display_options['filters']['status_extra']['group'] = 0;
  $handler->display->display_options['filters']['status_extra']['expose']['operator'] = FALSE;

  /* Display: Coming soon */
  $handler = $view->new_display('page', 'Coming soon', 'page_coming_soon');
  $handler->display->display_options['display_description'] = 'Page pour prochainement dans les salles.';
  $handler->display->display_options['path'] = 'cinema/prochainement';

  /* Display: Blockbusters */
  $handler = $view->new_display('page', 'Blockbusters', 'blockbusters_page');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Les films les plus attendus';
  $handler->display->display_options['display_description'] = 'Blockbuters page';
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Nodequeue: Queue */
  $handler->display->display_options['relationships']['nodequeue_rel']['id'] = 'nodequeue_rel';
  $handler->display->display_options['relationships']['nodequeue_rel']['table'] = 'node';
  $handler->display->display_options['relationships']['nodequeue_rel']['field'] = 'nodequeue_rel';
  $handler->display->display_options['relationships']['nodequeue_rel']['required'] = TRUE;
  $handler->display->display_options['relationships']['nodequeue_rel']['limit'] = 1;
  $handler->display->display_options['relationships']['nodequeue_rel']['names'] = array(
    'blockbusters' => 'blockbusters',
    'index_slideshow' => 0,
    'cinema_slideshow' => 0,
    'upcoming_movies' => 0,
    'this_week_movies' => 0,
  );
  $handler->display->display_options['path'] = 'cinema/blockbusters';

  /* Display: This week */
  $handler = $view->new_display('page', 'This week', 'page_1');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'A l\'affiche cette semaine';
  $handler->display->display_options['display_description'] = 'This week\'s movies';
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Nodequeue: Queue */
  $handler->display->display_options['relationships']['nodequeue_rel']['id'] = 'nodequeue_rel';
  $handler->display->display_options['relationships']['nodequeue_rel']['table'] = 'node';
  $handler->display->display_options['relationships']['nodequeue_rel']['field'] = 'nodequeue_rel';
  $handler->display->display_options['relationships']['nodequeue_rel']['required'] = TRUE;
  $handler->display->display_options['relationships']['nodequeue_rel']['limit'] = 1;
  $handler->display->display_options['relationships']['nodequeue_rel']['names'] = array(
    'this_week_movies' => 'this_week_movies',
    'index_slideshow' => 0,
    'cinema_slideshow' => 0,
    'upcoming_movies' => 0,
    'blockbusters' => 0,
  );
  $handler->display->display_options['path'] = 'cinema/cette-semaine';
  $translatables['cinema_coming_soon'] = array(
    t('Defaults'),
    t('Prochainement en salle'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« premier'),
    t('‹ précédent'),
    t('suivant ›'),
    t('dernier »'),
    t('Plus de films'),
    t('queue'),
    t('Coming soon'),
    t('Page pour prochainement dans les salles.'),
    t('Blockbusters'),
    t('Les films les plus attendus'),
    t('Blockbuters page'),
    t('This week'),
    t('A l\'affiche cette semaine'),
    t('This week\'s movies'),
  );
  $export['cinema_coming_soon'] = $view;

  return $export;
}
