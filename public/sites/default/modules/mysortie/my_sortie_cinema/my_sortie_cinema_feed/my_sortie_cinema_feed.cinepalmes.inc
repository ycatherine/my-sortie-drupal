<?php
/**
 * @file
 * Contains code for the Cinepalmes feeds.
 */

/**
 * Processes the Cinepalmes feed and returns all sessions.
 */
function _my_sortie_cinema_feed_get_cinepalmes_sessions() {

  // Instanciate our variables.
  $films = array();
  $sessions = array();

  // We first replace the encoding for the feed then simplexml load the string.
  $cinema_feed = str_replace('encoding="unicode"', 'encoding="utf-8"', MY_SORTIE_CINEMA_FEED_CINEPALMES);
  $xml_object = simplexml_load_string($cinema_feed);

  // Store films in an array, and discard the provider (Fournisseur > ICC).
  foreach ($xml_object as $object) {
    if (!empty($object)) {
      $films[] = $object;
    }
  }

  foreach ($films as $film) {

    // Save the title in a variable before looping through other items.
    $title = $film->titre;

    foreach ($film->salle as $location) {
      // Save the name of the Cinema in a variable before looping through
      // other items.
      $cinema = $location->nom;

      $location_array = (array)$location;
      $sessions_array = $location_array['horaire'];

      // Construct every single sessions here.
      if (isset($sessions_array)) {
        foreach ($sessions_array as $key => $session_time) {
          $sessions[] = array(
            'title' => reset($title),
            'cinema' => reset($cinema),
            'time' => strtotime($session_time),
          );
        }
      }
    }

  }

  return $sessions;
}
