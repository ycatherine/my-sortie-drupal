<?php
/**
 * @file
 * Contains helpers for the feeds module.
 */

/**
 * Delete all sessions.
 */
function _my_sortie_cinema_feed_delete_sessions() {
  $nids = db_select('node', 'n')
    ->fields('n', array('nid'))
    ->condition('type', array('session'))
    ->execute()
    ->fetchCol();

  node_delete_multiple($nids);
}


/**
 * Programmatically create a session.
 *
 * @param string $title.
 *   The session title (movie title).
 *
 * @param string $cinema.
 *   The cinema name.
 *
 * @param timestamp $time.
 *   The session time.
 */
function _my_sortie_cinema_feed_create_session($title, $cinema, $time) {

  $data = entity_create('node', array('type' => 'session'));
  $entity = entity_metadata_wrapper('node', $data);

  $entity->title = $title;
  $entity->field_cinema = $cinema;
  $entity->field_session = $time;

  $entity->save();
}
