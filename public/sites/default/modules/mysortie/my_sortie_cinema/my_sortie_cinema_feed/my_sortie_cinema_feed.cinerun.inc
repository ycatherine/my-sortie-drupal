<?php
/**
 * @file
 * Functions for Cine-Reunion feeds, takes care of storing sessions.
 */

/**
 * Returns an array of XML filenames.
 */
function _my_sortie_cinema_feed_get_cinerun_filenames() {

  return array(
    variable_get('my_sortie_cinema_cambaie', ''),
    variable_get('my_sortie_cinema_feed_lacaze', ''),
    variable_get('my_sortie_cinema_feed_plaza', ''),
  );

}

/**
 * Return the real value of the Cinema reference.
 *
 * @param string $reference.
 *   The reference of the Cinema.
 *
 * @return string.
 *   The cinema name.
 */
function _my_sortie_cinema_feed_cinema_name($reference) {

  switch ($reference) {
    case 'P9066':
      $cinema = 'Multiplexe Ciné Cambaie Saint-Paul';
      break;

    case 'P0826':
      $cinema = 'Ciné Lacaze Saint-Denis';
      break;

    case 'W9745':
      $cinema = 'Cinéma le Plaza Saint-Louis';
      break;
  }

  return $cinema;
}


/**
 * Processes the sessions for a feed.
 *
 * @param string $filename.
 *   The path to the xml file.
 *
 * @return array.
 *   An array of movie sessions.
 */
function _my_sortie_cinema_feed_process_sessions($filename) {

  if (empty($filename)) {
    return;
  }

  $sessions = array();
  $xml_object = simplexml_load_file($filename);

  foreach ($xml_object as $film) {
    $cinema = $xml_object->IdCine;
    $cinema_name = _my_sortie_cinema_feed_cinema_name(reset($cinema));
    $title = $film->Titre;

    foreach ($film->Seances as $seance) {

      foreach ($seance as $session_time) {
        $date_parts = explode('/', $session_time->DateSc, 3);
        $date = $date_parts[2] . '-' . $date_parts[1] . '-' . $date_parts[0];

        $sessions[] = array(
          'title' => reset($title),
          'cinema' => $cinema_name,
          'time' => strtotime($date . ' ' . $session_time->HeureSc),
        );
      }
    }
  }

  return $sessions;
}


/**
 * Returns an array containing all sessions.
 */
function _my_sortie_cinema_feed_get_cinerun_sessions() {

  $sessions = array();
  foreach (_my_sortie_cinema_feed_get_cinerun_filenames() as $filename) {
    $sessions = array_merge($sessions, _my_sortie_cinema_feed_process_sessions($filename));
  }

  return $sessions;
}
