<?php
/**
 * @file
 * Admin page callback file for My Sortie Cinema Feed.
 */

/**
 * Creates and display the Cinema Feed configuration form.
 */
function my_sortie_cinema_feed_admin_form() {
  $form = array();

  // Flux Cinépalmes.
  $form['my_sortie_cinema_feed_icc'] = array(
    '#type' => 'textarea',
    '#title' => t('Cinépalmes'),
    '#rows' => 20,
    '#description' => t('Source code du flux Cinépalmes. <a href="http://www.invescorun.com/flux/xmlparfilm.xml">Flux XML par Film.</a>.'),
    '#default_value' => variable_get('my_sortie_cinema_feed_icc', ''),
  );

  // Flux Ciné Réunion.
  $form['my_sortie_cinema_cambaie'] = array(
    '#type' => 'textfield',
    '#title' => t('Cambaie | P0966'),
    '#description' => t('Flux Cambaie - voir http://cine-reunion.com/cine_flux/'),
    '#default_value' => variable_get('my_sortie_cinema_cambaie', ''),
  );

  $form['my_sortie_cinema_feed_lacaze'] = array(
    '#type' => 'textfield',
    '#title' => t('Lacaze | P0826'),
    '#description' => t('Flux Plaza - voir http://cine-reunion.com/cine_flux/'),
    '#default_value' => variable_get('my_sortie_cinema_feed_lacaze', ''),
  );

  $form['my_sortie_cinema_feed_plaza'] = array(
    '#type' => 'textfield',
    '#title' => t('Plaza | W9745'),
    '#description' => t('Flux Plaza - voir http://cine-reunion.com/cine_flux/'),
    '#default_value' => variable_get('my_sortie_cinema_feed_plaza', ''),
  );

  return system_settings_form($form);
}
