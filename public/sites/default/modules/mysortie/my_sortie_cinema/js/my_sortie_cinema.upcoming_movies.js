(function ($) {

  /**
   * Bootstrap upcoming movies vue template.
   */
  Drupal.behaviors.mySortieCinemaUpComingMovies = {
    attach: function (context, settings) {

      Vue.component('movie-grid-upcoming', {
        template: '#movie-grid-template-upcoming',
        replace: true,
        props: ['data', 'filter-key'],
        data: function () {
          return {
            data: null,
            filterKey: ''
          }
        }
      });

      var upComingMoviesGrid = new Vue({
        el: '#movie-grid-upcoming',
        data: {
          searchQuery: '',
          gridData: Drupal.settings.mySortieCinemaUpcomingMovies
        }
      })
    }
  };

})(jQuery);
