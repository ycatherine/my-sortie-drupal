(function ($) {

  /**
   * Bootstrap this week vue template.
   */
  Drupal.behaviors.mySortieCinemaThisWeek = {
    attach: function (context, settings) {

      Vue.component('movie-grid', {
        template: '#movie-grid-template',
        replace: true,
        props: ['data', 'filter-key'],
        data: function () {
          return {
            data: null,
            filterKey: ''
          }
        }
      })

      var moveGrid = new Vue({
        el: '#movie-grid',
        data: {
          searchQuery: '',
          gridData: settings.mySortieCinemaWeek
        }
      })
    }
  };

})(jQuery);
