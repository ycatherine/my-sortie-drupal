<?php
/**
 * @file
 * Contains helpers for the My Sortie Cinema Session module.
 */

/**
 * Get the sessions for the movie we are viewing.
 *
 * @param string $matching_title.
 *   The title matching the XML feed.
 *
 * @param string $secondary_title.
 *   A secondary matching title.
 *
 * @return object.
 *   The DatabaseConnection_mysql object.
 */
function _my_sortie_cinema_sessions_per_movie($matching_title, $secondary_title) {

  $query = db_select('node', 'n');
  $query->join('field_data_field_cinema', 'fc', 'n.nid = fc.entity_id');
  $query->join('field_data_field_session', 'fs', 'n.nid = fs.entity_id');

  $result = $query
    ->fields('n', array('nid'))
    ->fields('fc', array('field_cinema_value'))
    ->fields('fs', array('field_session_value'))
    ->condition('type', 'session', '=')
    ->condition('status', NODE_PUBLISHED, '=');

  $db_or = db_or();
  $db_or->condition('title', $matching_title, '=');
  $db_or->condition('title', $secondary_title, '=');
  $result->condition($db_or);

  return $result->execute();
}
