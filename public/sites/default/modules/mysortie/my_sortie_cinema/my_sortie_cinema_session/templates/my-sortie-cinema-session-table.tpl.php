<?php
/**
 * @file
 * Template for movie sessions stored in a table.
 */
?>
<div class="session">
  <h2 class="session__heading"><?php print $title; ?></h2>
  <table class="session__table">
    <tbody>
    <?php foreach ($sessions as $day => $session): ?>
      <tr class="session__rows">
        <td class="session__day"><?php print $day; ?></td>
        <td class="session__times">
          <?php foreach ($session as $time): ?>
            <span class="session_time"><?php print $time['time']; ?></span>
          <?php endforeach; ?>
        </td>
      </tr>
    <?php endforeach; ?>
    </tbody>
  </table>
</div>