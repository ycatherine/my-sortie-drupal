<?php
/**
 * @file
 * my_sortie_field.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function my_sortie_field_taxonomy_default_vocabularies() {
  return array(
    'event_types' => array(
      'name' => 'Types d\'évènement',
      'machine_name' => 'event_types',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'location_type' => array(
      'name' => 'Types de lieu',
      'machine_name' => 'location_type',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
