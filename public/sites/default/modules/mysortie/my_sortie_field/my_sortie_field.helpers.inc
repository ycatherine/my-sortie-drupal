<?php
/**
 * @file
 * Contains helpers for retrieving field values.
 */

/**
 * Fetches a single value from a field.
 *
 * @param object $entity.
 *   The entity containing the data to be displayed.

 * @param string $field_name.
 *   The field to be displayed.
 *
 * @param string $key.
 *   The key of value to be displayed.
 *
 * @param string $entity_type.
 *   The type of $entity; e.g., 'node' or 'user'.
 *
 * @return string.
 *   Returns a field value, or FALSE.
 */
function _my_sortie_field_get_value($entity, $field_name, $key = 'value', $entity_type = 'node') {

  if (!$field_value = field_get_items($entity_type, $entity, $field_name)) {
    return FALSE;
  }

  return reset($field_value)[$key];
}


/**
 * Retrieves the node title from the entity reference.
 *
 * @param object $entity.
 *   The entity containing the data to be displayed.

 * @param string $field_name.
 *   The field to be displayed.
 *
 * @return array.
 *   Returns an array of node titles from the entity reference.
 */
function _my_sortie_field_get_entity_reference_title($entity, $field_name) {

  if (!$field_value = field_get_items('node', $entity, $field_name)) {
    return FALSE;
  }

  $titles = array();
  $cardinality = field_info_field($field_name)['cardinality'];

  if ($cardinality == -1 || $cardinality > 1) {
    foreach ($field_value as $target) {
      $titles[] = node_load($target['target_id'])->title;
    }
  }

  return $titles;
}


/**
 * Retrieves the term ID form an taxonomy term reference field.
 *
 * @param object $entity.
 *   The entity containing the data to be displayed.

 * @param string $field_name.
 *   The field to be displayed.
 *
 * @return array.
 *   Returns an array of tids from the term reference.
 */
function _my_sortie_field_get_tid_reference($entity, $field_name) {

  if (!$field_values = field_get_items('node', $entity, $field_name)) {
    return FALSE;
  }

  $tids = array();

  foreach ($field_values as $field) {
    $tids[] = $field['tid'];
  }

  return $tids;
}
