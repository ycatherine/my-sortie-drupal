<?php
/**
 * @file
 * my_sortie_event.quicktabs.inc
 */

/**
 * Implements hook_quicktabs_default_quicktabs().
 */
function my_sortie_event_quicktabs_default_quicktabs() {
  $export = array();

  $quicktabs = new stdClass();
  $quicktabs->disabled = FALSE; /* Edit this to true to make a default quicktabs disabled initially */
  $quicktabs->api_version = 1;
  $quicktabs->machine_name = 'events_listing';
  $quicktabs->ajax = 0;
  $quicktabs->hide_empty_tabs = FALSE;
  $quicktabs->default_tab = 0;
  $quicktabs->title = 'Events listing';
  $quicktabs->tabs = array(
    0 => array(
      'vid' => 'events_listing',
      'display' => 'block_1',
      'args' => '',
      'title' => 'Ce Week end',
      'weight' => '-100',
      'type' => 'view',
    ),
    1 => array(
      'vid' => 'events_listing',
      'display' => 'events_week',
      'args' => '',
      'title' => '7 Prochains jours',
      'weight' => '-99',
      'type' => 'view',
    ),
    2 => array(
      'vid' => 'events_listing',
      'display' => 'block_30_days',
      'args' => '',
      'title' => '30 Prochains jours',
      'weight' => '-98',
      'type' => 'view',
    ),
  );
  $quicktabs->renderer = 'quicktabs';
  $quicktabs->style = 'nostyle';
  $quicktabs->options = array();

  // Translatables
  // Included for use with string extractors like potx.
  t('30 Prochains jours');
  t('7 Prochains jours');
  t('Ce Week end');
  t('Events listing');

  $export['events_listing'] = $quicktabs;

  return $export;
}
