<?php
/**
 * @file
 * my_sortie_event.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function my_sortie_event_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "quicktabs" && $api == "quicktabs") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function my_sortie_event_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_custom_pub_defaults().
 */
function my_sortie_event_custom_pub_defaults() {
  $options = array();
  // Exported option: this_month
  $options['this_month'] = array(
    'type' => 'this_month',
    'name' => t('30 prochains jours'),
    'node_types' => array(
      'event' => t('Evenement'),
    ),
  );

  // Exported option: this_week
  $options['this_week'] = array(
    'type' => 'this_week',
    'name' => t('7 prochains jours'),
    'node_types' => array(
      'event' => t('Evenement'),
    ),
  );

  // Exported option: this_weekend
  $options['this_weekend'] = array(
    'type' => 'this_weekend',
    'name' => t('Ce weekend'),
    'node_types' => array(
      'event' => t('Evenement'),
    ),
  );

  return $options;
}

/**
 * Implements hook_node_info().
 */
function my_sortie_event_node_info() {
  $items = array(
    'event' => array(
      'name' => t('Evenement'),
      'base' => 'node_content',
      'description' => t('Ajouter un évènement.'),
      'has_title' => '1',
      'title_label' => t('Titre'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
