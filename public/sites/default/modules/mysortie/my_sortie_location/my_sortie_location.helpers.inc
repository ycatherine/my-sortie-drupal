<?php
/**
 * @file
 * Helpers for My Sortie location.
 */

/**
 * Returns nodes from the location content type ordered by title.
 */
function _my_sortie_location_nodes() {
  $query = new EntityFieldQuery();

  $query
    ->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'location')
    ->propertyCondition('status', 1)
    ->propertyOrderBy('title', 'ASC');
  $result = $query->execute();

  return array_keys($result['node']);
}


/**
 * Returns all location titles.
 */
function _my_sortie_location_metadata() {
  if (!module_load_include('inc', 'my_sortie_field', 'my_sortie_field.helpers')) {
    return;
  }

  $locations = node_load_multiple(_my_sortie_location_nodes());
  $location_names = array();

  foreach ($locations as $location) {
    $title = $location->title;
    $nid   = $location->nid;
    $references = _my_sortie_field_get_tid_reference($location, 'field_location_types');

    foreach ($references as $reference) {
      if ($term = taxonomy_term_load($reference)) {
        $term = $term->name;
      }
    }

    $location_names[] = l($title . ' (' . $term . ')', 'node/' . $nid);
  }

  return $location_names;
}
