<?php
/**
 * @file
 * my_sortie_location.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function my_sortie_location_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'blocks_location';
  $context->description = 'Location page blocks.';
  $context->tag = 'blocks';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'lieux' => 'lieux',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-location_map-block_location_map' => array(
          'module' => 'views',
          'delta' => 'location_map-block_location_map',
          'region' => 'content',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Location page blocks.');
  t('blocks');
  $export['blocks_location'] = $context;

  return $export;
}
