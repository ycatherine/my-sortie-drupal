<?php
/**
 * Movie card custom template.
 */
?>
<div class="movie-card">
  <?php print render($content['field_image']); ?>

  <h3 class="movie-card__title"><?php print $title; ?></h3>
</div>
<!-- node__view_mode_movie_card -->