<?php
/**
 * @file
 * Custom theme implementation of the Thumbnail view mode.
 */
?>
<div class="thumbnail">
  <a class="thumbnail__link" href="<?php print $node_url; ?>/lightbox" title="<?php print $title; ?>">
    <?php print render($content['field_image']); ?>
  </a>
  <h3 class="thumbnail__title"><a href="<?php print $node_url; ?>/lightbox" title="<?php print $title; ?>"><?php print $title; ?></a></h3>
</div>
