<?php
/**
 * @file
 * Custom theme implementation of the Card view mode.
 */
?>
<a class="card" href="<?php print $node_url; ?>" title="<?php print $title; ?>">

  <div class="card__image-container">
    <div class="card__image"><?php print render($content['field_image']); ?></div>
    <div class="card__image-overlay"></div>
    <h2 class="card__title">
      <?php print $title; ?>
      <?php if ($is_partner): ?>
        <span class="card__partner"><i class="icon--star"></i> Partenaire</span>
      <?php endif; ?>
      <?php if ($win_places): ?>
        <span class="card__win"><i class="icon--stack"></i> Gagner des places</span>
      <?php endif; ?>
    </h2>
  </div>

  <div class="card__content">

    <?php if (isset($content['field_date'])): ?>
      <div class="card__date">
        <div class="iconic">
          <i class="iconic__icon icon--calendar"></i>
          <div class="iconic__text">
            <?php print render($content['field_date']); ?>
          </div>
        </div>
      </div>
    <?php endif; ?>

    <?php if (isset($content['field_location'])): ?>
      <div class="card__location">
        <div class="iconic">
          <i class="iconic__icon icon--pin"></i>
          <div class="iconic__text"><?php print render($content['field_location']); ?></div>
        </div>
      </div>
    <?php endif; ?>
    <div class="card__tag">
      <div class="iconic">
        <i class="iconic__icon icon--tag"></i>
        <div class="iconic__text"><?php print render($content['field_event_type']); ?></div>
      </div>
    </div>
  </div>

  <div class="card__cta">En savoir plus</div>

  <?php if (isset($date_from_field_collection)): ?>
    <?php print $date_from_field_collection; ?>
  <?php endif; ?>

</a> <!-- /.node--view-mode-card -->
