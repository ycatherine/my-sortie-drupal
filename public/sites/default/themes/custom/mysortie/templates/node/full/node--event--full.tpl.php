<?php
/**
 * @file
 * Custom theme implementation of an event full view mode.
 */
?>
<div class="event" itemscope itemtype="http://schema.org/Event">
  <div class="event__container">

    <div class="event__image margin-bottom">
      <div class="masthead">
        <div class="masthead__overlay"></div>
        <div class="masthead__first">
          <?php print render($content['field_image']); ?>
        </div>

        <div class="masthead__second">
          <div class="masthead__title-prefix"><?php print render($content['field_event_type']); ?></div>
          <h1 class="masthead__title" itemprop="name"><?php print $title; ?></h1>
          <div class="masthead__title-suffix">
            <?php if (isset($content['field_date'])): ?>
              <time datetime="<?php print $datetime_attribute; ?>" itemprop="startDate" content="<?php print $datetime_content; ?>">
                <?php print render($content['field_date']); ?>
              </time>
            <?php endif; ?>
            <?php if (isset($location_name)): ?>
            • <?php print $location_name; ?>
            <?php endif; ?>
          </div>
        </div>
      </div> <!-- /.masthead -->
    </div> <!-- /.event__image -->

    <div class="layout layout--two-col">

      <div class="layout__first">
        <div class="event__description margin-bottom" itemprop="description">
          <div class="text-indent"><?php print render($content['body']); ?></div>
        </div>

        <?php if (isset($content['field_location'])): ?>
          <div class="event__map margin-bottom">
            <?php print render($map); ?>
          </div>
        <?php endif; ?>
      </div> <!-- /.layout-first -->

      <div class="layout__second margin-bottom--large">
        <div class="sidebar">
          <h2 class="sidebar__heading">Détails</h2>
          <time class="iconic margin-bottom" datetime="<?php print $datetime_attribute; ?>" itemprop="startDate" content="<?php print $datetime_content; ?>">
            <i class="iconic__icon icon--calendar icon--large"></i>
            <div class="iconic__text"><?php print $field_date_value; ?></div>
          </time>

          <div class="iconic margin-bottom">
            <i class="iconic__icon icon--pin icon--large"></i>
            <div class="iconic__text"><?php print render($location_name); ?></div>
            <?php if (!empty($location_address)): ?>
              <?php print render($location_address); ?>
            <?php endif; ?>
          </div>

          <?php if (isset($location_phone) && !empty($location_phone)): ?>
            <div class="iconic margin-bottom">
              <i class="iconic__icon icon--phone icon--large"></i>
              <div class="iconic__text"><?php print $location_phone; ?></div>
            </div>
          <?php endif; ?>

          <?php if (isset($content['field_price'])): ?>
            <div class="iconic margin-bottom">
              <i class="iconic__icon icon--bill icon--large"></i>
              <div class="iconic__text"><?php print render($content['field_price']); ?></div>
            </div>
          <?php endif; ?>

          <?php if ($win_places): ?>
            <div class="margin-bottom">
              <a class="button button--secondary button--full" href="<?php print $win_places_url; ?>"><i class="icon--stack margin-right"></i>Gagner des places</a>
            </div>
          <?php endif; ?>
        </div> <!-- /.sidebar -->

        <?php if (isset($mediascope_ad_sidebar)): ?>
          <div class="advert margin-bottom">
            <?php print render($mediascope_ad_sidebar); ?>
          </div>
        <?php endif; ?>

        <?php if (isset($content['field_online_booking'])): ?>
          <h2 class="sidebar__heading">Réservation</h2>
          <?php print render($content['field_online_booking']); ?>
        <?php endif; ?>
      </div> <!-- /.layout__second -->

    </div> <!-- /.layout -->

  </div> <!-- /.event__container -->
</div> <!-- /.node--event--full -->

<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-55a6430d2674c9fa" async="async"></script>

