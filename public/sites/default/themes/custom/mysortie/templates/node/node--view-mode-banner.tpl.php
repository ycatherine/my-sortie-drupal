<?php
/**
 * @file
 * Custom theme implementation of the Banner view mode.
 */
?>
<div class="banner">

  <div class="banner__first">
    <?php print render($content['field_image']); ?>
    <div class="banner__overlay"></div>
  </div> <!-- /.banner__first -->

  <div class="banner__second">
    <div class="wrapper wrapper--medium text-align-center">
      <h1 class="banner__title"><?php print $title; ?></h1>
      <?php if ($content['body']): ?>
        <div class="banner__body"><?php print render($content['body']); ?></div>
      <?php endif; ?>

      <?php if ($content['field_link']): ?>
        <div class="banner__link"><?php print render($content['field_link']); ?></div>
      <?php endif; ?>
    </div>
  </div> <!-- /.banner__second -->

</div> <!-- /.node--view-mode-banner -->
