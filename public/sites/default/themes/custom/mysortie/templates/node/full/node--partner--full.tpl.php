<?php
/**
 * @file
 * Custom implementation of a partner node.
 */
?>
<div class="partner">
  <div class="parter__image">
    <?php print render($content['field_image']); ?>
  </div>
</div>