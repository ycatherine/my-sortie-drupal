<?php
/**
 * @file
 * Custom implementation of a webform in full view.
 */
?>
<div class="page-title--banner margin-bottom--large">
  <div class="wrapper wrapper--large">
    <h1 class="page-title__title"><?php print $title; ?></h1>
  </div>
</div>

<div class="webform">
  <?php if (isset($content['field_image'])): ?>
    <div class="image-mask--circle text-align-center margin-bottom--large">
      <?php print render($content['field_image']); ?>
    </div>
  <?php endif; ?>

  <div class="wrapper text-align-center margin-bottom--large">
    <?php if (isset($content['body'])): ?>
      <?php print render($content['body']); ?>
    <?php endif; ?>
  </div>

  <div class="wrapper wrapper--medium">
    <div class="form--dark">
      <?php print render($content['webform']); ?>
    </div>
  </div>

</div>
