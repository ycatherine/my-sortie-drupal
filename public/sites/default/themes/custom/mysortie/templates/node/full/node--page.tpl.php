<?php
/**
 * Custom implementation of a page.
 */
?>
<div class="basic-page wrapper">
  <?php print render($content); ?>
</div>
<!-- node__page -->