<?php
/**
 * @file
 * Custom implementation of the jumbotron view mode.
 */
?>
<div class="jumbotron">

  <div class="jumbotron__background visible--medium">
    <?php print render($content['field_banner']); ?>

    <div class="jumbotron__overlay"></div>
  </div>

  <div class="jumbotron__content">
    <div class="wrapper">
      <div class="jumbotron__first">
        <h2 class="jumbotron__title"><?php print $title; ?></h2>
        <div class="jumbotron__body visible--small">
          <?php print render($content['body']); ?>
        </div>
        <ul class="jumbotron__meta">
          <li><strong>Genre(s) : </strong><?php print render($content['field_genres']); ?></li>
          <li><strong>Acteurs : </strong><?php print render($content['field_actors']); ?></li>
          <li><strong>Date de sortie : </strong><?php print $release_date; ?></li>
        </ul>
      </div>

      <div class="jumbotron__second">

        <?php if (!empty($youtube_url)): ?>
          <div class="trailer-thumb jumbotron__trailer">
            <a href="<?php print $youtube_url; ?>" class="trailer-thumb__link popup-youtube">
              <?php print render($content['field_image']); ?>
              <div class="trailer-thumb__button"></div>
            </a>
          </div>
        <?php endif; ?>

        <a href="<?php print $node_url; ?>" class="button button--secondary button--full">En savoir plus</a>
      </div>

    </div>
  </div>

</div>
