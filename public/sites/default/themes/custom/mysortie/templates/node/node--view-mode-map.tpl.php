<?php
/**
 * @file
 * Custom theme implementation of a map view mode.
 */
?>
<?php if (isset($content['field_lat_long'])): ?>
  <div class="map">
    <?php print render($content['field_lat_long']); ?>
  </div>
<?php endif; ?>
