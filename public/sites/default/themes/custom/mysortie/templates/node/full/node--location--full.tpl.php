<?php
/**
 * @file
 * Custom theme implementation of an location full view mode.
 */
?>
<div class="location">

  <div class="map margin-bottom">
    <?php print render($content['field_lat_long']); ?>
  </div> <!-- /.margin-bottom -->

  <div class="layout layout--two-col">

    <div class="layout__first">
      <div class="margin-bottom">
        <h1 class="page-title"><?php print $title; ?></h1>
        <?php if (isset($content['field_location_types'])): ?>
          <h2 class="page-title__subtitle"><?php print render($content['field_location_types']); ?></h2>
        <?php endif; ?>
      </div>

      <?php if (isset($content['field_address'])
        || (isset($content['field_phone_number']))
        || (isset($content['field_email']))
        || (isset($content['field_website']))):
      ?>
      <div class="margin-bottom">
        <?php if (isset($content['field_address'])): ?>
          <div class="location__address">
            <strong>Adresse : </strong>
            <address><?php print render($content['field_address']); ?></address>
          </div>
        <?php endif; ?>

        <?php if (isset($content['field_phone_number'])): ?>
          <div class="location__phone">
            <strong>Téléphone : </strong>
            <span><?php print render($content['field_phone_number']); ?></span>
          </div>
        <?php endif; ?>

        <?php if (isset($content['field_email'])): ?>
          <div class="location__email">
            <strong>Email : </strong>
            <span><?php print render($content['field_email']); ?></span>
          </div>
        <?php endif; ?>

        <?php if (isset($content['field_website'])): ?>
          <div class="location__website">
            <strong>Site : </strong>
            <a href="<?php print $website; ?>" title="Lien vers <?php print $title; ?>"><?php print $website; ?></a>
          </div>
        <?php endif; ?>
      </div>
      <?php endif; ?>

      <?php if (isset($content['body'])): ?>
        <div class="margin-bottom location__description">
          <?php print render($content['body']); ?>
        </div>
      <?php endif; ?>

      <?php if (!empty($album_location)): ?>
        <div class="margin-bottom">
          <h2 class="font-h3">
            <i class="icon--image"></i>
            <span>En images</span>
          </h2>
          <?php print $album_location; ?>
        </div>
      <?php endif; ?>

    </div> <!-- /.layout__first -->

    <div class="layout__second">
      <?php if (!empty($events_per_location)): ?>
        <div class="margin-bottom">
          <h2 class="sidebar__heading">Programmation à venir</h2>
          <?php print $events_per_location; ?>
        </div>
      <?php endif; ?>

      <?php if (isset($mediascope_ad_sidebar)): ?>
        <div class="advert margin-bottom--large">
          <?php print render($mediascope_ad_sidebar); ?>
        </div>
      <?php endif; ?>
    </div> <!-- /.layout__second -->

  </div> <!-- /.layout -->

</div> <!-- /.location -->