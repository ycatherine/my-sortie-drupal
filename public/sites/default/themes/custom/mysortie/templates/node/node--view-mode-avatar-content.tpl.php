<?php
/**
 * @file
 * Custom theme implementation of the Avatar Content view mode.
 */
?>
<div class="avatar-content">
  <div class="avatar-content__first">
    <div class="avatar-content__image"><?php print render($content['field_image']); ?></div>
  </div>

  <div class="avatar-content__second">
    <h3 class="avatar-content__title">
      <a class="avatar-content__link" href="<?php print $node_url; ?>" title="<?php print $title; ?>">
        <?php print $title; ?>
      </a>
    </h3>
    <time class="avatar-content__date" datetime="<?php print $datetime_attribute; ?>"><?php print $field_date_value; ?></time>
    <div class="avatar-content__type"><?php print render($content['field_event_type']); ?></div>
  </div>
</div> <!-- /.node--view-mode-avatar-content -->
