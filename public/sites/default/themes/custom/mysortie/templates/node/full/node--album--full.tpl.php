<?php
/**
 * @file
 * Custom theme implementation of an album full view mode.
 */
?>
<div class="page-title--banner margin-bottom--large">
  <div class="wrapper wrapper--large">
    <h1 class="page-title__title"><?php print $title; ?></h1>
  </div>
</div>

<div class="album margin-bottom--large">
  <div class="wrapper wrapper--large">
    <?php if (isset($content['body'])): ?>
      <div class="album__body font-h5 text-align-center margin-bottom--large">
        <?php print render($content['body']); ?>
      </div>
    <?php endif; ?>
  </div>

  <div class="layout wrapper wrapper--large">
    <div class="layout--two-col">

      <div class="layout__first">
        <?php if (isset($content['field_images'])): ?>
          <div class="album__gallery lightbox-field">
            <?php print render($content['field_images']); ?>
          </div>
        <?php endif; ?>

        <?php if (isset($content['field_photo_disclaimer'])): ?>
          <div class="margin-bottom">
            <em><?php print render($content['field_photo_disclaimer']); ?></em>
          </div>
        <?php endif; ?>
      </div>

      <div class="layout__second">
        <?php if (isset($mediascope_ad_sidebar)): ?>
          <div class="advert margin-bottom--large">
            <?php print render($mediascope_ad_sidebar); ?>
          </div>
        <?php endif; ?>
      </div>

    </div>
  </div>
</div> <!-- /.node--album--full -->

<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-55a6430d2674c9fa" async="async"></script>
