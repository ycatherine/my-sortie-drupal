<?php
/**
 * @file
 * Custom theme implementation of the Panel view mode.
 */
?>
<a class="panel" href="<?php print $field_link_value; ?>" title="<?php print $title; ?>">

  <div class="panel__image">
    <?php print render($content['field_image']); ?>
  </div>

  <div class="panel__content">
    <h3 class="panel__title"><?php print $title; ?></h3>
    <div class="panel__sub-title">
      <?php if (isset($content['field_sub_title'])): ?>
        <?php print render($content['field_sub_title']); ?>
      <?php endif; ?>

      <?php if (isset($content['body'])): ?>
        <?php print render($content['body']); ?>
      <?php endif; ?>
    </div>
  </div>

</a> <!-- /.node--view-mode-panel -->
