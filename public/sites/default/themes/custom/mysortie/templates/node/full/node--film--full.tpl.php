<?php
/**
 * @file
 * Custom implementation of a film in full view.
 */
?>
<div class="film" itemscope itemtype="http://schema.org/Movie">
  <div class="banner">

    <div class="banner__first">
      <?php if (isset($content['field_banner'])): ?>
        <?php print render($content['field_banner']); ?>
      <?php endif; ?>
      <div class="banner__overlay"></div>
    </div> <!-- /.banner__first -->

    <div class="banner__second">

      <div class="wrapper wrapper--medium text-align-center">
        <h1 class="banner__title" itemprop="name"><?php print $title; ?></h1>

        <div class="banner__body">

          <div class="banner__meta">
            <?php if (isset($content['field_duration'])): ?>
              <div class="banner__icon">
                <div class="iconic">
                  <i class="iconic__icon icon--clock"></i>
                  <time class="iconic__text" itemprop="duration" datetime="<?php print $datetime_duration; ?>"><?php print render($content['field_duration']); ?> min</time>
                </div>
              </div>
            <?php endif; ?>

            <?php if (isset($content['field_genres'])): ?>
              <div class="banner__icon">
                <div class="iconic">
                  <i class="iconic__icon icon--tag"></i>
                  <div class="iconic__text"><?php print $genres; ?></div>
                </div>
              </div>
            <?php endif; ?>

            <?php if (isset($content['field_date'])): ?>
              <div class="banner__icon">
                <div class="iconic">
                  <i class="iconic__icon icon--calendar"></i>
                  <div class="iconic__text" itemprop="datePublished" content="<?php print $movie_date_attribute; ?>">
                    <?php print $movie_date; ?>
                  </div>
                </div>
              </div>
            <?php endif; ?>
          </div>

        </div>
      </div>

    </div>
  </div> <!-- /.banner -->

  <div class="layout layout--three-col">

    <div class="layout__first">
      <?php if (isset($content['field_image'])): ?>
        <div class="margin-bottom hidden--x-small" itemprop="image">
          <?php print render($content['field_image']); ?>
        </div>
      <?php endif; ?>

      <?php if (isset($content['field_actors'])): ?>
        <div class="margin-bottom" itemscope itemtype="http://schema.org/Person">
          <div class="label"><strong>Acteurs</strong></div>
          <?php print $actors; ?>
        </div>
      <?php endif; ?>

      <?php if (isset($content['field_directors'])): ?>
        <div class="margin-bottom" itemscope itemtype="http://schema.org/Person">
          <div class="label"><strong>Réalisateur(s)</strong></div>
          <?php print $directors; ?>
        </div>
      <?php endif; ?>

      <?php if (isset($content['field_country'])): ?>
        <div class="margin-bottom">
          <div class="label"><strong>Nationalité</strong></div>
          <?php print render($content['field_country']); ?>
        </div>
      <?php endif; ?>

      <?php if (isset($content['field_year'])): ?>
        <div class="margin-bottom">
          <div class="label"><strong>Année de production</strong></div>
          <?php print render($content['field_year']); ?>
        </div>
      <?php endif; ?>
    </div>

    <div class="layout__second">
      <?php if (isset($content['field_youtube'])): ?>
        <div class="margin-bottom">
          <?php print render($content['field_youtube']); ?>
        </div>
      <?php endif; ?>

      <?php if (isset($content['body'])): ?>
        <div class="margin-bottom--large">
          <h3>Synopsis</h3>
          <div itemprop="description"><?php print render($content['body']); ?></div>
        </div>
      <?php endif; ?>

      <?php if (isset($my_sortie_cinema_session_sessions)): ?>
        <h3>Les séances</h3>
        <div class="margin-bottom--large">
          <?php print render($my_sortie_cinema_session_sessions); ?>
        </div>
      <?php endif; ?>

    </div>

    <div class="layout__third">
      <?php if (isset($mediascope_ad_sidebar)): ?>
        <div class="advert margin-bottom--large">
          <?php print render($mediascope_ad_sidebar); ?>
        </div>
      <?php endif; ?>
    </div>

  </div> <!-- /.layout--three-col -->
</div> <!-- /node--film--full -->

<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-55a6430d2674c9fa" async="async"></script>
