<?php
/**
 * @file
 * Custom theme implementation of the Address view mode.
 */
?>
<address class="address"  itemprop="location" itemscope itemtype="http://schema.org/PostalAddress">
  <?php print render($content['field_address']); ?>
</address> <!-- /.node__view_mode_address -->
