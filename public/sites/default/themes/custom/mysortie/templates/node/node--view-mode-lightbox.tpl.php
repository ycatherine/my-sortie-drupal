<?php
/**
 * @file
 * Custom theme implementation of the Lightbox view mode.
 */
?>
<div class="lightbox wrapper">
  <div class="lightbox__first">
    <?php print render($content['field_image']); ?>
  </div>

  <div class="lightbox__second">
    <h2 class="lightbox__title"><?php print $title; ?></h2>
    <h3 class="lightbox__subtitle">La photo du jour</h3>

    <?php if (isset($content['body'])): ?>
      <p class="lightbox__body"><?php print render($content['body']); ?></p>
    <?php endif; ?>

    <?php if (isset($content['field_photo_disclaimer'])): ?>
      <p class="lightbox__displaimer"><em><?php print render($content['field_photo_disclaimer']); ?></em></p>
    <?php endif; ?>

    <a class="button button--primary margin-bottom" href="<?php print $node_url; ?>">Voir l'évènement en image</a>

  </div>
</div> <!-- /.node--view-mode-lightbox -->
