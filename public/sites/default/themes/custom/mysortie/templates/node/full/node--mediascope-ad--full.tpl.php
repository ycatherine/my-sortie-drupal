<?php
/**
 * @file
 * Custom template for an advert.
 */
?>
<?php if (isset($content['field_mediascope_ad_link'])) : ?>
  <a href="<?php print render($content['field_mediascope_ad_link']); ?>" target="_blank" onclick="trackOutboundLink('<?php print render($content['field_mediascope_ad_link']); ?>'); return false;">
    <?php print render($content['field_mediascope_ad_image']); ?>
  </a>
  <?php else: ?>
  <?php print render($content['field_mediascope_ad_image']); ?>
<?php endif; ?>
