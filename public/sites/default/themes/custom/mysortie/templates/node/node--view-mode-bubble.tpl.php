<?php
/**
 * @file
 * Custom implementation of the bubble view mode.
 */
?>
<div class="bubble text-align-center">
  <?php if (isset($content['field_image'])): ?>
    <div class="bubble__image margin-bottom">
      <div class="image-mask--circle">
        <?php print render($content['field_image']); ?>
      </div>
    </div>
  <?php endif; ?>

  <div class="bubble__content margin-bottom">
    <?php if (isset($content['body'])): ?>
      <div class="margin-bottom">
        <?php print render($content['body']); ?>
      </div>
    <?php endif; ?>

    <a href="<?php print $node_url; ?>" class="bubble__button button button--secondary text-align-center">En savoir plus</a>
  </div>
</div>