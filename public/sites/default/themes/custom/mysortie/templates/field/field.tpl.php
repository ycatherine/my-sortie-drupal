<?php

/**
 * @file field.tpl.php
 * Custom theme implementation of a field.
 */
?>
<?php foreach ($items as $delta => $item): ?>
  <?php print render($item); ?>
<?php endforeach; ?>
