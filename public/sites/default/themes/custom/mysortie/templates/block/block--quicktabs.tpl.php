<?php

/**
 * @file
 * Custom implementation to display a Quicktabs block.
*/
?>
<div class="quicktabs wrapper wrapper--large margin-bottom--large">
  <?php print $content; ?>
</div> <!-- ./block--quicktabs -->
