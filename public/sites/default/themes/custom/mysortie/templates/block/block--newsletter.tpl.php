<?php

/**
 * @file
 * Custom implementation to display the newsletter block.
 */
?>
<div class="newsletter margin-bottom">
  <div class="wrapper wrapper--large">

    <?php if ($block->subject): ?>
      <h2 class="newsletter__title"><i class="icon--newspaper"></i>
        <span><?php print $block->subject ?></span>
      </h2>
    <?php endif; ?>
    <div class="wrapper">
      <?php print $content; ?>
      <em class="newsletter__disclaimer">* Champ obligatoire</em>
    </div>
  </div>
</div> <!-- /.block--newsletter -->
