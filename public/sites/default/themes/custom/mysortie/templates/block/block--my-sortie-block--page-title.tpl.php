<?php
/**
 * @file
 * Custom implementation of the Page title block from My Sortie Block module.
 */
?>
<div class="page-title--banner margin-bottom--large">
  <div class="wrapper wrapper--large">
    <h1 class="page-title__title"><?php print $content; ?></h1>
  </div>
</div> <!-- /.block--my-sortie-block--page-title -->
