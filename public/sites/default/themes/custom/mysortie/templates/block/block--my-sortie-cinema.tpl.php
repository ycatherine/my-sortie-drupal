<?php

/**
 * @file
 * Custom implementation to display a Cinema block.
 */
?>
<div class="wrapper wrapper--large margin-bottom--large">
  <?php print $content; ?>
</div> <!-- ./block--my-sortie-cinema -->
