<?php

/**
 * @file
 * Custom implementation to display a block coming from the album view.
 */
?>
<div<?php print $attributes; ?>>
  <?php print render($title_prefix); ?>
  <?php if ($block->subject): ?>
    <h2 class="font-h3">
      <i class="icon--image"></i>
      <span><?php print $block->subject; ?></span>
    </h2>
  <?php endif; ?>
  <?php print render($title_suffix); ?>
  <?php print $content; ?>
</div> <!-- /.block--views--album-block-album -->
