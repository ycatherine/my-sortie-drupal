<div class="page-404 wrapper wrapper--large text-align-left">
  <h1>Whoops ! Erreur 404 <br/>Page introuvable</h1>
  <p class="margin-bottom"><strong>La page demandée n'a pas pu être trouvée</strong></p>
  <a href="/" class="button button--secondary margin-bottom">Accéder à la page d'accueil</a> <a href="/nous-contacter" class="button button--secondary margin-bottom">Nous contacter</a>
</div>
