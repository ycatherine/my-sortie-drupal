<?php

/**
 * @file
 * Custom theme implementation to display a single Drupal page.
 */
?>

<?php if (!empty($page['ad_top'])): ?>
  <div class="advert advert--dark">
    <div class="wrapper wrapper--medium">
      <?php print render($page['ad_top']); ?>
    </div>
  </div>
<?php endif; ?>

<div id="sb-site" class="page">

  <header class="header" role="banner">

    <div class="mobile-header hidden--large">
      <div class="mobile-header__container">
        <?php if ($logo): ?>
          <a href="<?php print $front_page; ?>" title="<?php print t('Les sorties Cinéma et Culture sur la Réunion'); ?>" rel="home" class="mobile-header__logo"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" /></a>
        <?php endif; ?>

        <a class="mobile-header__link sb-toggle-left text-align-right text-small-caps" href="#slidebars">
          <i class="icon icon--list"></i> <?php print t('Menu'); ?>
        </a>
      </div>
    </div>

    <div class="header__top navbar visible--large">
      <div class="wrapper wrapper--large">
        <?php if ($logo): ?>
          <a href="<?php print $front_page; ?>" title="<?php print t('Les sorties Cinéma et Culture sur la Réunion'); ?>" rel="home" class="header__logo site-logo"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" /></a>
        <?php endif; ?>

        <nav class="header__site-switcher nav nav--tab" role="navigation">
          <?php print render($site_switcher_menu); ?>
        </nav>

        <nav class="header__utility-menu nav nav--right" role="navigation">
          <?php print render($utility_menu); ?>
        </nav>
      </div>
    </div> <!-- /.navbar -->

    <?php if ($page['navigation']): ?>
      <div class="navbar navbar--light navbar--sticky visible--large">
        <div class="wrapper wrapper--large">
          <div class="nav">
            <?php print render($page['navigation']); ?>
          </div>
        </div>
      </div> <!-- /.navbar -->
    <?php endif; ?>

  </header> <!-- /.header -->

  <?php if ($page['banner']): ?>
    <div class="banner-region">
      <?php print render($page['banner']); ?>
    </div> <!-- /.banner-region -->
  <?php endif; ?>

  <div class="margin-bottom--large">
    <div class="main-content">
      <a id="main-content"></a>
      <?php print render($page['content']); ?>
    </div> <!-- /.main-content -->

    <?php if ($page['content_suffix']): ?>
      <div class="margin-bottom--large layout wrapper--large">

        <div class="layout--two-col">
          <div class="layout__first">
            <div class="grid-content">
              <?php print render($page['content_suffix']); ?>
            </div>
          </div>

          <div class="layout__second">
            <?php if (!empty($page['ad_sidebar'])): ?>
              <div class="advert advert--sidebar">
                <?php print render($page['ad_sidebar']); ?>
              </div>
            <?php endif; ?>

            <?php print render($page['content_suffix_sidebar']); ?>
          </div>
        </div>
      </div>
    <?php endif; ?>

    <?php if (!empty($page['ad_bottom'])): ?>
      <div class="advert">
        <div class="wrapper wrapper--medium">
          <?php print render($page['ad_bottom']); ?>
        </div>
      </div>
    <?php endif; ?>

  </div>

  <footer class="footer">

    <div class="wrapper">
      <div class="footer__first">
        <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
      </div>

      <div class="footer__second">
        <div class="footer__second-col-first">
          <h3 class="footer__heading">Sorties</h3>
          <?php print render($menu_sorties); ?>
        </div>

        <div class="footer__second-col-second">
          <h3 class="footer__heading">Cinéma</h3>
          <?php print render($menu_cinema); ?>
        </div>

        <div class="footer__second-col-third">
          <h3 class="footer__heading">Nos réseaux sociaux</h3>
          <ul class="reset-list social social--light">
            <li><a class="social__link" href="<?php print $socials['facebook']; ?>" target="_blank"><i class="icon--facebook icon--large"></i></a></li>
          </ul>
        </div>

      </div>
      <div class="footer__menu">
        <?php print render($footer_menu); ?>
      </div>
      <p class="footer__copyright">Copyright <?php print date('Y'); ?> Le Quotidien | Tous droits réservés</p>
    </div>

  </footer>
</div>

<div class="back-to-top"><i class="icon--arrow-up"></i> Retour en haut</div>

<div id="slidebars" class="sb-slidebar sb-left">
  <div class="mobile-menu">
    <div class="mobile-menu__content">
      <div class="mobile-menu__menu">
        <?php print render($full_site_menu); ?>
        <?php print render($utility_menu); ?>
      </div>
    </div>
  </div> <!-- /.mobile-menu -->
</div>
