<?php

/**
 * @file
 * Template overrides as well as (pre-)process and alter hooks for the
 * My Sortie theme.
 */

/**
 * Get a field value.
 *
 * @param object $entity.
 *   The entity containing the data to be displayed.

 * @param string $field_name.
 *  The field to be displayed.
 *
 * @param string $key.
 *   The key of value to be displayed.
 *
 * @return string.
 *   Returns the value of field, or NULL if not found.
 */
function _mysortie_get_field_value($entity, $field_name, $key = 'value') {

  if (!$field_value = field_get_items('node', $entity, $field_name)) {
    return NULL;
  }

  $field_item = reset($field_value);
  return $field_item[$key];
}


/**
 * Returns an array of library we need on all/most pages.
 */
function _mysortie_libraries() {
  return array(
    'magnific_popup',
    'vuejs',
    'slidebars',
    'sticky',
  );
}


/**
 * View fetcher.
 *
 * @param string $name.
 *   The machine name of the view to embed.
 *
 * @param string $display_id.
 *   The display ID of the view.
 *
 * @param integer $arg.
 *   The view argument.
 *
 * @return string.
 *   The view content.
 */
function _my_sortie_embed_view($name, $display_id, $arg = '') {
  $result = views_get_view_result($name, $display_id);
  return count($result) ? views_embed_view($name, $display_id) : '';
}


/**
 * Comma separated itemprop items.
 */
function _my_sortie_item_prop_spans($values, $attributes) {
  $prefix = '';
  $output = '';

  foreach ($values as $value) {
    $span = theme('html_tag', array(
      'element' => array(
        '#tag' => 'span',
        '#attributes' => $attributes,
        '#value' => $value,
      ),
    ));

    $output .= trim($prefix . $span);
    $prefix = ", ";
  }

  return $output;
}
