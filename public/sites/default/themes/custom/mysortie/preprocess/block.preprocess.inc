<?php
/**
 * @file
 * Preprocess blocks.
 */

/**
 * Implements hook_preprocess_block().
 */
function mysortie_preprocess_block(&$variables) {

  $newsletter_block_delta = 'client-block-12';

  if ($variables['block']->delta === $newsletter_block_delta) {
    $variables['theme_hook_suggestions'][] = 'block__newsletter';
  }

}
