<?php
/**
 * @file
 * Preprocess nodes.
 */

/**
 * Implements hook_preprocess_node().
 */
function mysortie_preprocess_node(&$variables) {

  // Helper variables.
  $view_mode = $variables['view_mode'];
  $node = $variables['node'];
  $nid = $node->nid;


  // Custom view mode templates.
  $variables['theme_hook_suggestions'][] = 'node__view_mode_' . $variables['view_mode'];
  $variables['theme_hook_suggestions'][] = 'node__' . $variables['type'] . '__' . $variables['view_mode'];

  // Create a datetime attribute.
  $field_date_value = _mysortie_get_field_value($variables['node'], 'field_date');
  // Datetime attribute for the <time> HTML tag.
  $variables['datetime_attribute'] = $field_date_value ? format_date($field_date_value, 'custom', 'Y-m-d G:i') : '';
  // Datetime content attribute for the schema.
  $variables['datetime_content'] = $field_date_value ? format_date($field_date_value, 'custom', 'Y-m-d\TG:i') : '';
  // The field date value in a custom format. e.g. Monday 13 2015 - 19:00.
  $variables['field_date_value'] = $field_date_value ? format_date($field_date_value, 'custom', 'l j F Y - G:i') : '';


  // Link from the link field.
  $field_link_value = _mysortie_get_field_value($variables['node'], 'field_link', 'url');
  $variables['field_link_value'] = $field_link_value ? $field_link_value : '';


  // Variables for the location content type on full view mode.
  if ($node->type == 'location' && $variables['view_mode'] == 'full') {
    // Events per location view.
    $variables['events_per_location'] = _my_sortie_embed_view('events_per_location', 'events_per_location_block', $nid);

    // Album per location view.
    $variables['album_location'] = _my_sortie_embed_view('album', 'block_location_images', $nid);

    // The valid website address.
    $website = _mysortie_get_field_value($node, 'field_website');
    $variables['website'] = '';

    if (isset($website) && valid_url($website)) {
      $variables['website'] = $website;
    }
  }


  // @todo: Refactor this to the my_sortie_film feature once created.
  if ($node->type == 'film' && ($view_mode == 'full')) {
    // The datetime attribute for duration.
    if ($duration = _mysortie_get_field_value($node, 'field_duration')) {
      // e.g. PT100M.
      $variables['datetime_duration'] = 'PT' . filter_xss($duration) . 'M';
    }

    // Genres.
    $variables['genres'] = '';

    if ($genres = field_get_items('node', $node, 'field_genres')) {
      $info = field_info_field('field_genres');
      $values = $info['settings']['allowed_values'];

      foreach ($genres as $genre) {
        $genres_list[] = $values[$genre['value']];
      }

      $genres_attributes = array('#itemprop' => 'genre');
      $variables['genres'] = _my_sortie_item_prop_spans($genres_list, $genres_attributes);
    }

    // Date published (movie).
    $movie_date = _mysortie_get_field_value($node, 'field_date');
    $variables['movie_date_attribute'] = $movie_date ? format_date($movie_date, 'custom', 'Y-m-d') : '';
    $variables['movie_date'] = $movie_date ? format_date($movie_date, 'custom', 'j F Y') : '';

    // The actors.
    $variables['actors'] = '';
    if ($actors_list = field_get_items('node', $node, 'field_actors')) {

      foreach ($actors_list as $actor) {
        $actors[] = $actor['safe_value'];
      }

      $actors_attributes = array('#itemprop' => 'name');
      $variables['actors'] = _my_sortie_item_prop_spans($actors, $actors_attributes);
    }

    // Directors.
    $variables['director'] = '';
    if ($directors_list = field_get_items('node', $node, 'field_directors')) {

      foreach ($directors_list as $director) {
        $directors[] = $director['safe_value'];
      }
    }

    $directors_attributes = array('#itemprop' => 'name');
    $variables['directors']= _my_sortie_item_prop_spans($directors, $directors_attributes);
  }


  // Jumbotron stuff.
  if ($node->type == 'film' && ($view_mode == 'jumbotron')) {

    // The Youtube URL.
    $youtube = _mysortie_get_field_value($node, 'field_youtube', 'input');
    $variables['youtube_url'] = (!is_null($youtube)) ? $youtube : '';

    // Release date.
    $release_date = _mysortie_get_field_value($node, 'field_date');
    $variables['release_date'] = (!is_null($release_date)) ? format_date($release_date, 'custom', 'l j F Y') : '';
  }
}
