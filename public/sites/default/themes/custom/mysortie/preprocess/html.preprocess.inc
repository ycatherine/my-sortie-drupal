<?php
/**
 * @file
 * Preprocess HTML for My Sortie theme.
 */

/**
 * Implements hook_preprocess_html().
 */
function mysortie_preprocess_html(&$variables) {

  //Adds typekit.
  drupal_add_js('//use.typekit.net/kay0zgl.js', 'external');
  drupal_add_js('try{Typekit.load();}catch(e){}', 'inline', 'page_bottom');

  if (drupal_get_http_header('status') === '404 Not Found') {
    $variables['classes_array'][] = 'body-404';
  }
}
