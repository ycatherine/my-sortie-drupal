<?php
/**
 * @file
 * Preprocess Page for My Sortie theme.
 */

/**
 * Implements hook_preprocess_page().
 */
function mysortie_preprocess_page(&$variables) {

  $path_to_theme = base_path() . drupal_get_path('theme', 'mysortie');

  $variables['socials'] = array(
    'facebook' => 'https://www.facebook.com/Mysortiere-279305270128',
    'youtube'  => 'https://youtube.com',
  );

  // Load libraries needed on all/most pages.
  if (module_exists('my_sortie_library')) {
    foreach (_mysortie_libraries() as $library) {
      libraries_load($library);
    }
  }

  // Unset home page content.
  if (drupal_is_front_page()) {
    unset($variables['page']['content']['system_main']);
  }

  // Unset content on taxonomy pages.
  if (arg(0) == 'taxonomy' && arg(1) == 'term' && is_numeric(arg(2))){
    unset($variables['page']['content']['system_main']['nodes']);
    unset($variables['page']['content']['system_main']['pager']);
    unset($variables  ['page']['content']['system_main']['no_content']);
  }

  // Main menu tree.
  $variables['utility_menu'] = menu_tree('main-menu');

  // Secondary menu / Footer menu.
  $footer_menu = menu_build_tree('menu-secondary-menu', array('max_depth' => 1));
  $variables['footer_menu'] = menu_tree_output($footer_menu);

  // Site Switcher menu.
  $site_switcher = menu_build_tree('menu-site', array('max_depth' => 1));
  $variables['site_switcher_menu'] = menu_tree_output($site_switcher);

  // Full site menu.
  $full_site_menu = menu_build_tree('menu-site', array('min_depth' => 1, 'max_depth' => 2));
  $variables['full_site_menu'] = menu_tree_output($full_site_menu);

  // Menu sorties & cinema
  $variables['menu_sorties'] = menu_tree('menu-sorties');
  $variables['menu_cinema']  = menu_tree('menu-menu-cinema');

  // Path to Quotidien logo.
  $variables['quotidien_light'] = $path_to_theme . '/images/le-quotidien-light.png';

  if (drupal_get_http_header('status') === '404 Not Found') {
    $variables['theme_hook_suggestions'][] = 'page__404';
  }
}
