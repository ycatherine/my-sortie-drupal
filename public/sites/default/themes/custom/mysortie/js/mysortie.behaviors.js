(function ($) {

  /**
   * Album lightbox.
   */
  Drupal.behaviors.mysortieAlbumLightbox = {
    attach: function (context, settings) {

      $('.thumbnail__link', context).once('thumbnail').magnificPopup({
        type: 'ajax',
        gallery:{
          enabled:true
        },
        callbacks: {
          parseAjax: function(mfpResponse) {
            mfpResponse.data = $(mfpResponse.data).find('.lightbox');
          },
          ajaxContentAdded: function() {

          }
        }
      });
    }
  };


  /**
   * Youtube lightbox.
   */
  Drupal.behaviors.mysortieYoutubeLightbox = {
    attach: function (context, settings) {

      $('.popup-youtube', context).once('popup-youtube').magnificPopup({
        disableOn: 700,
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: false,
        fixedContentPos: false
      });

    }
  };


  /**
   * Lightbox field.
   */
  Drupal.behaviors.mysortieLightboxField = {
    attach: function (context, settings) {

      $('.lightbox-field a', context).once('lightbox-field').magnificPopup({
        type: 'image',
        gallery:{
          enabled:true
        }
      });
    }
  };

  /**
   * Initializes slidebars.
   *
   * @see hook_libraries_info() in my_sortie_library.
   */
  Drupal.behaviors.mySortieSlidebars = {
    attach: function (context) {
      $.slidebars();
    }
  };


  /**
   * Back to top fadein/out
   */
  Drupal.behaviors.mySortieBackToTop = {
    attach: function (context, settings){

      $(window).scroll(function () {
        if ($(this).scrollTop() > 0) {
          $('.back-to-top', context).fadeIn();
        }
        else {
          $('.back-to-top', context).fadeOut();
        }
      });

      // scroll body to 0px on click
      $('.back-to-top', context).once('scroller').click(function () {
        $('body, html').animate({
          scrollTop: 0
        }, 400);
        return false;
      });

    }
  };


  /**
   * Sticky navs.
   *
   * @see sticky library https://github.com/garand/sticky.
   */
  Drupal.behaviors.mySortieStickyNav = {
    attach: function (context, settings){

      // Check nav tabs are available.
      var $context =  $('.navbar--sticky', context);

      if ($context.length == 0) {
        return;
      }

      // Initialise Sticky.
      $context.once('sticky-nav').sticky({
        topSpacing: 0
      });

    }
  };

})(jQuery);
